###############################################################################
#
#  Shorewall Version 5 -- /etc/shorewall/shorewall.conf
#
#  For information about the settings in this file, type "man shorewall.conf"
#
#  Manpage also online at http://www.shorewall.net/manpages/shorewall.conf.html
###############################################################################
#		       S T A R T U P   E N A B L E D
###############################################################################

STARTUP_ENABLED=Yes

###############################################################################
#			     V E R B O S I T Y
###############################################################################

VERBOSITY=1

###############################################################################
#			        P A G E R
###############################################################################

PAGER=pager

###############################################################################
#			     F I R E W A L L
###############################################################################

FIREWALL=

###############################################################################
#			       L O G G I N G
###############################################################################

LOG_LEVEL="NFLOG(0,64,1)"

BLACKLIST_LOG_LEVEL="none"

INVALID_LOG_LEVEL=

LOG_BACKEND=netlink

LOG_MARTIANS=Yes

LOG_VERBOSITY=1

LOGALLNEW=

LOGFILE=/var/log/ulogd/ulogd.syslogemu.log

LOGFORMAT=": %s %s"

LOGTAGONLY=Yes

LOGLIMIT="s:5/min"

MACLIST_LOG_LEVEL="$LOG_LEVEL"

RELATED_LOG_LEVEL="$LOG_LEVEL:,related"

RPFILTER_LOG_LEVEL="$LOG_LEVEL:,rpfilter"

SFILTER_LOG_LEVEL="$LOG_LEVEL"

SMURF_LOG_LEVEL="$LOG_LEVEL"

STARTUP_LOG=/var/log/shorewall-init.log

TCP_FLAGS_LOG_LEVEL="$LOG_LEVEL"

UNTRACKED_LOG_LEVEL=

###############################################################################
#	L O C A T I O N	  O F	F I L E S   A N D   D I R E C T O R I E S
###############################################################################

ARPTABLES=

CONFIG_PATH=":/etc/shorewall:/usr/share/shorewall:/usr/share/shorewall/Shorewall"

GEOIPDIR=/usr/share/xt_geoip/LE

IPTABLES=/sbin/iptables

IP=/sbin/ip

IPSET=

LOCKFILE=/var/lib/shorewall/lock

MODULESDIR="+extra/RTPENGINE"

NFACCT=

PATH="/usr/local/sbin:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin"

PERL=/usr/bin/perl

RESTOREFILE=

SHOREWALL_SHELL=/bin/sh

SUBSYSLOCK=

TC=

###############################################################################
#		D E F A U L T   A C T I O N S / M A C R O S
###############################################################################

ACCEPT_DEFAULT="none"
BLACKLIST_DEFAULT="NotSyn(DROP):$LOG_LEVEL"
DROP_DEFAULT="Broadcast(DROP),Multicast(DROP)"
NFQUEUE_DEFAULT="none"
QUEUE_DEFAULT="none"
REJECT_DEFAULT="Broadcast(DROP),Multicast(DROP)"

###############################################################################
#			 R S H / R C P	C O M M A N D S
###############################################################################

RCP_COMMAND='scp ${files} ${root}@${system}:${destination}'
RSH_COMMAND='ssh ${root}@${system} ${command}'

###############################################################################
#			F I R E W A L L	  O P T I O N S
###############################################################################

ACCOUNTING=Yes

ACCOUNTING_TABLE=mangle

ADD_IP_ALIASES=No

ADD_SNAT_ALIASES=No

ADMINISABSENTMINDED=Yes

AUTOCOMMENT=Yes

AUTOHELPERS=No

AUTOMAKE=Yes

BALANCE_PROVIDERS=No

BASIC_FILTERS=No

BLACKLIST="NEW,INVALID,UNTRACKED"

CLAMPMSS=No

CLEAR_TC=Yes

COMPLETE=No

DEFER_DNS_RESOLUTION=No

DELETE_THEN_ADD=No

DETECT_DNAT_IPADDRS=No

DISABLE_IPV6=No

DOCKER=No

DONT_LOAD="nf_nat_sip,nf_conntrack_sip,nf_conntrack_h323,nf_nat_h323"

DYNAMIC_BLACKLIST="ipset-only,disconnect,timeout=7200"

EXPAND_POLICIES=Yes

EXPORTMODULES=Yes

FASTACCEPT=Yes

FORWARD_CLEAR_MARK=No

HELPERS="ftp,irc"

IGNOREUNKNOWNVARIABLES=No

IMPLICIT_CONTINUE=No

INLINE_MATCHES=No

IPSET_WARNINGS=Yes

IP_FORWARDING=Yes

KEEP_RT_TABLES=Yes

LOAD_HELPERS_ONLY=Yes

MACLIST_TABLE=filter

MACLIST_TTL=60

MANGLE_ENABLED=Yes

MARK_IN_FORWARD_CHAIN=No

MINIUPNPD=No

MULTICAST=No

MUTEX_TIMEOUT=60

NULL_ROUTE_RFC1918=unreachable

OPTIMIZE=All

OPTIMIZE_ACCOUNTING=No

PERL_HASH_SEED=12345

REJECT_ACTION=

REQUIRE_INTERFACE=No

RESTART=restart

RESTORE_DEFAULT_ROUTE=No

RESTORE_ROUTEMARKS=Yes

RETAIN_ALIASES=No

ROUTE_FILTER=No

SAVE_ARPTABLES=No

SAVE_IPSETS=ipv4

TC_ENABLED=No

TC_EXPERT=No

TC_PRIOMAP="2 3 3 3 2 3 1 1 2 2 2 2 2 2 2 2"

TRACK_PROVIDERS=Yes

TRACK_RULES=No

USE_DEFAULT_RT=Yes

USE_NFLOG_SIZE=Yes

USE_PHYSICAL_NAMES=Yes

USE_RT_NAMES=Yes

VERBOSE_MESSAGES=No

WARNOLDCAPVERSION=Yes

WORKAROUNDS=No

ZERO_MARKS=No

ZONE2ZONE=-

###############################################################################
#			P A C K E T   D I S P O S I T I O N
###############################################################################

BLACKLIST_DISPOSITION=DROP

INVALID_DISPOSITION=CONTINUE

MACLIST_DISPOSITION=ACCEPT

RELATED_DISPOSITION=REJECT

RPFILTER_DISPOSITION=DROP

SMURF_DISPOSITION=DROP

SFILTER_DISPOSITION=DROP

TCP_FLAGS_DISPOSITION=DROP

UNTRACKED_DISPOSITION=DROP

################################################################################
#			P A C K E T  M A R K  L A Y O U T
################################################################################

TC_BITS=8

PROVIDER_BITS=2

PROVIDER_OFFSET=16

MASK_BITS=8

ZONE_BITS=0
