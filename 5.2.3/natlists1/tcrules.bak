#
# Shorewall version 3.0 - Tcrules File
#
# /etc/shorewall/tcrules
#
#	Entries in this file cause packets to be marked as a means of
#	classifying them for traffic control or policy routing.
#
#			       I M P O R T A N T ! ! ! !
#
#		Unlike rules in the /etc/shorewall/rules file, evaluation
#		of rules in this file will continue after a match. So the
#		final mark for each packet will be the one assigned by the
#		LAST tcrule that matches.
#
#		If you use multiple internet providers with the 'track' option,
#		in /etc/shorewall/providers be sure to read the restrictions at
#		http://shorewall.net/Shorewall_and_Routing.html.
#
# Columns are:
#
#
#	MARK/		a) A mark value which is an integer in the range 1-255
#	CLASSIFY
#			   May optionally be followed by ":P" or ":F"
#			   where ":P" indicates that marking should occur in
#			   the PREROUTING chain and ":F" indicates that marking
#			   should occur in the FORWARD chain. If neither
#			   ":P" nor ":F" follow the mark value then the chain
#			   is determined by the setting of
#			   MARK_IN_FORWARD_CHAIN in
#			   /etc/shorewall/shorewall.conf.
#
#			   If your kernel and iptables include CONNMARK support
#			   then you can also mark the connection rather than
#			   the packet.
#
#			   The mark value may be optionally followed by "/"
#			   and a mask value (used to determine those bits of
#			   the connection mark to actually be set). The
#			   mark and optional mask are then followed by one of:
#
#				C - Mark the connection in the chain determined
#				    by the setting of MARK_IN_FORWARD_CHAIN
#
#				CF: Mark the connection in the FORWARD chain
#
#				CP: Mark the connection in the PREROUTING
#				    chain.
#
#			b) A classification (classid) of the form
#                          <major>:<minor> where <major> and <minor> are
#                          integers. Corresponds to the 'class' specification
#                          in these traffic shaping modules:
#
#				- atm
#				- cbq
#				- dsmark
#				- pfifo_fast
#				- htb
#				- prio
#
#			   Classify always occurs in the POSTROUTING chain.
#
#			c) RESTORE[/mask] -- restore the packet's mark from the
#			   connection's mark using the supplied mask if any.
#			   Your kernel and iptables must include CONNMARK
#			   support.
#
#			   As in a) above, may be followed by ":P" or ":F
#
#			c) SAVE[/mask] -- save the packet's mark to the
#			   connection's mark using the supplied mask if any.
#			   Your kernel and iptables must include CONNMARK
#			   support.
#
#			   As in a) above, may be followed by ":P" or ":F
#
#			d) CONTINUE -- don't process any more marking rules in
#			   the table.
#
#			   As in a) above, may be followed by ":P" or ":F".
#
#	SOURCE		Source of the packet. A comma-separated list of
#			interface names, IP addresses, MAC addresses
#			and/or subnets. If your kernel and iptables include
#			iprange match support, IP address ranges are also
#			allowed. Use $FW if the packet originates on
#			the firewall in which case the MARK column may NOT
#			specify either ":P" or ":F" (marking always occurs
#			in the OUTPUT chain). $FW may be optionally followed
#			by ":" and a host/network address.
#
#			MAC addresses must be prefixed with "~" and use
#			"-" as a separator.
#
#			Example: ~00-A0-C9-15-39-78
#
#	DEST		Destination of the packet. Comma separated list of
#			IP addresses and/or subnets. If your kernel and
#			iptables include iprange match support, IP address
#			ranges are also allowed.
#
#			If the MARK column specificies a classification of
#			the form <major>:<minor> then this column may also
#			contain an interface name.
#
#	PROTO		Protocol - Must be "tcp", "udp", "icmp", "ipp2p",
#                       "ipp2p:udp", "ipp2p:all" a number, or "all".
#                       "ipp2p" requires ipp2p match support in your kernel
#                       and iptables.
#
#	PORT(S)		Destination Ports. A comma-separated list of Port
#			names (from /etc/services), port numbers or port
#			ranges; if the protocol is "icmp", this column is
#			interpreted as the destination icmp-type(s).
#
#			If the protocol is ipp2p, this column is interpreted
#			as an ipp2p option without the leading "--" (example
#			"bit" for bit-torrent). If no PORT is given, "ipp2p" is
#			assumed.
#
#			This column is ignored if PROTOCOL = all but must be
#			entered if any of the following field is supplied.
#			In that case, it is suggested that this field contain
#			 "-"
#
#	SOURCE PORT(S)	(Optional) Source port(s). If omitted,
#			any source port is acceptable. Specified as a comma-
#			separated list of port names, port numbers or port
#			ranges.
#
#	USER		This column may only be non-empty if the SOURCE is
#			the firewall itself.
#
#			When this column is non-empty, the rule applies only
#			if the program generating the output is running under
#			the effective user and/or group.
#
#			It may contain :
#
#	[<user name or number>]:[<group name or number>][+<program name>]
#
#			The colon is optionnal when specifying only a user
#			or a program name.
#			Examples : john: , john , :users , john:users ,
#				   +mozilla-bin
#
#	TEST		Defines a test on the existing packet or connection
#			mark. The rule will match only if the test returns
#			true. Tests have the format [!]<value>[/<mask>][:C]
#
#			Where:
#
#				!	Inverts the test (not equal)
#				<value> Value of the packet or connection mark.
#				<mask>	A mask to be applied to the mark before
#					testing
#				:C	Designates a connection mark. If
#					omitted, the packet mark's value is
#					tested.
#
# See http://shorewall.net/traffic_shaping.htm for additional information.
# For usage in selecting among multiple ISPs, see
# http://shorewall.net/Shorewall_and_Routing.html
###############################################################################
#MARK	SOURCE		DEST		PROTO	PORT(S)	CLIENT	USER	TEST
#							PORT(S)
3       0.0.0.0/0       0.0.0.0/0       tcp     22
3       0.0.0.0/0       0.0.0.0/0       tcp     -       22
4       0.0.0.0/0       0.0.0.0/0       tcp     22      -       -       -       -       8
4       0.0.0.0/0       0.0.0.0/0       tcp     -       22      -       -       -       8
#LAST LINE -- ADD YOUR ENTRIES BEFORE THIS ONE -- DO NOT REMOVE
