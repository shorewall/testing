#
# Shorewall -- /etc/shorewall/rules
#
# For information on the settings in this file, type "man shorewall-rules"
#
# The manpage is also online at
# http://www.shorewall.net/manpages/shorewall-rules.html
#
##############################################################################################################################################################
#ACTION		SOURCE		DEST		PROTO	DPORT	SPORT	ORIGDEST	RATE	USER	MARK	CONNLIMIT	TIME	HEADERS	SWITCH	HELPER

?SECTION ALL

Ping(ACCEPT)	{ SOURCE=net, DEST=all, RATE=d:ping:2/sec:10 }
Trcrt(ACCEPT)	{ SOURCE=net, DEST=all, RATE=d:ping:2/sec:10 }

?SECTION ESTABLISHED

?SECTION RELATED

ACCEPT		{ SOURCE=all, DEST=dmz:$SERVER, PROTO=tcp,  DPORT=61001:62000,  helper=ftp }
ACCEPT		{ SOURCE=dmz, DEST=all,		PROTO=tcp,  helper=ftp }
ACCEPT		{ SOURCE=all, DEST=net,		PROTO=tcp,  helper=ftp }
ACCEPT		{ SOURCE=$FW, DEST=loc,		PROTO=tcp,  helper=ftp }
ACCEPT		{ SOURCE=loc, DEST=$FW,		PROTO=tcp,  helper=ftp }
ACCEPT		{ SOURCE=all, DEST=all,		PROTO=icmp }
RST(ACCEPT)	{ SOURCE=all, DEST=all }
ACCEPT		{ SOURCE=dmz, DEST=dmz }

?SECTION INVALID

RST(ACCEPT)	{ SOURCE=all, DEST=all }
FIN(ACCEPT)	{ SOURCE=all, DEST=all }
DROP		{ SOURCE=net, DEST=all }

?SECTION UNTRACKED

?if __IPV4
Broadcast(ACCEPT) { SOURCE=all, DEST=$FW }
ACCEPT		  { SOURCE=all, DEST=$FW, PROTO=udp }
CONTINUE	  { SOURCE=loc, DEST=$FW }
CONTINUE	  { SOURCE=$FW, DEST=all }
?endif

?SECTION NEW

######################################################################################################
# Stop certain outgoing traffic to the net
#
REJECT:$LOG_LEVEL { SOURCE=loc,vpn,apps DEST=net, PROTO=tcp, DPORT=25 }		#Stop direct loc->net SMTP (Comcast uses submission).
REJECT:$LOG_LEVEL { SOURCE=loc,vpn,apps DEST=net, PROTO=udp, DPORT=1025:1031 }	#MS Messaging

REJECT		{ SOURCE=all, DEST=net, PROTO=tcp, DPORT=137,445, comment="Stop NETBIOS Crap" }
REJECT		{ SOURCE=all, DEST=net, PROTO=udp, DPORT=137:139, comment="Stop NETBIOS Crap" }

REJECT		{ SOURCE=all, DEST=net, PROTO=tcp, DPORT=3333, comment="Disallow port 3333" }

REJECT		{ SOURCE=all, DEST=net, PROTO=udp, DPORT=3544, comment="Stop Teredo" }

?COMMENT

######################################################################################################
# 6in4
#
?if __IPV4
    ACCEPT		    { SOURCE=net:216.218.226.238, DEST=$FW, PROTO=41 }
    ACCEPT		    { SOURCE=$FW, DEST=net:216.218.226.238, PROTO=41 }
?endif
######################################################################################################
# Ping
#
Ping(ACCEPT)	  { SOURCE=$FW,loc,dmz,vpn,apps, DEST=$FW,loc,dmz,vpn,apps }
Ping(ACCEPT)	  { SOURCE=all,              DEST=net }
######################################################################################################
# SSH
#
AutoBL(SSH,60,-,-,-,-,$LOG_LEVEL)\
		  { SOURCE=net,              DEST=all,         PROTO=tcp, DPORT=22 }
SSH(ACCEPT)	  { SOURCE=all, DEST=all }
?if __IPV4
SSH(DNAT-)	  { SOURCE=net,              DEST=172.20.2.44, PROTO=tcp, DPORT=ssh, ORIGDEST=70.90.191.123 }
?endif
######################################################################################################
# DNS
#
DNS(ACCEPT)	  { SOURCE=loc,dmz,vpn,apps, DEST=$FW }
DNS(ACCEPT)	  { SOURCE=$FW, DEST=net }
######################################################################################################
# Traceroute
#
Trcrt(ACCEPT)	  { SOURCE=all, DEST=net }
Trcrt(ACCEPT)	  { SOURCE=net, DEST=$FW,dmz }
######################################################################################################
# Email
#
SMTP(ACCEPT)	   { SOURCE=net,$FW, DEST=dmz:$LISTS }
SMTP(ACCEPT)	   { SOURCE=dmz:$LISTS, DEST=net:PROD_IF }
SMTP(REJECT)	   { SOURCE=dmz:$LISTS, DEST=net }
IMAPS(ACCEPT)  	   { SOURCE=all, DEST=dmz:$MAIL }
Submission(ACCEPT) { SOURCE=all, DEST=dmz:$LISTS }
SMTPS(ACCEPT) 	   { SOURCE=all, DEST=dmz:$LISTS }
IMAP(ACCEPT)	   { SOURCE=loc,vpn, DEST=net }
######################################################################################################
# NTP
#
NTP(ACCEPT)	   { SOURCE=all, DEST=net }
######################################################################################################
# Squid
ACCEPT { SOURCE=loc,vpn, DEST=$FW, PROTO=tcp, DPORT=3128 } 
######################################################################################################
# HTTP/HTTPS
#
Web(ACCEPT)	   { SOURCE=loc,vpn DEST=$FW }
Web(ACCEPT)	   { SOURCE=$FW, DEST=net, USER=proxy }
Web(DROP)	   { SOURCE=net, DEST=fw, PROTO=tcp, comment="Do not blacklist web crawlers" }
HTTP(ACCEPT)	   { SOURCE=net,loc,vpn,$FW DEST=dmz:$SERVER,$LISTS,$MAIL }
HTTPS(ACCEPT)	   { SOURCE=net,loc,vpn,$FW DEST=dmz:$SERVER,$LISTS,$MAIL }
Web(ACCEPT)	   { SOURCE=dmz,apps DEST=net,$FW }
Web(ACCEPT)	   { SOURCE=$FW, DEST=net, USER=root }
Web(ACCEPT)	   { SOURCE=$FW, DEST=net, USER=teastep }
Web(ACCEPT)	   { SOURCE=$FW, DEST=net, USER=_apt }
######################################################################################################
# FTP
#
FTP(ACCEPT)	   { SOURCE=loc,vpn,apps  DEST=net }
FTP(ACCEPT)	   { SOURCE=dmz,          DEST=net }
FTP(ACCEPT)	   { SOURCE=$FW,          DEST=net, USER=root }
FTP(ACCEPT)	   { SOURCE=all,          DEST=dmz:$SERVER }
#
# Some FTP clients seem prone to sending the PORT command split over two packets.
# This prevents the FTP connection tracking code from processing the command  and setting
# up the proper expectation.
#
# The following rule allows active FTP to work in these cases
# but logs the connection so I can keep an eye on this potential security hole.
#
ACCEPT:$LOG_LEVEL  { SOURCE=dmz, DEST=net, PROTO=tcp, DPORT=1024:, SPORT=20 }
######################################################################################################
# whois
#
Whois(ACCEPT)	   { SOURCE=all, DEST=net }
######################################################################################################
# SMB
#
SMBBI(ACCEPT)	    { SOURCE=loc, DEST=$FW }
SMBBI(ACCEPT)	    { SOURCE=vpn, DEST=$FW }
######################################################################################################
# IRC
#
IRC(ACCEPT)	    { SOURCE=loc,apps, DEST=net }
######################################################################################################
# AUTH
Auth(REJECT)	{ SOURCE=net, DEST=all }
######################################################################################################
# Rsync
#
Mirrors(ACCEPT:none) { SOURCE=net, DEST=dmz:$SERVER, PROTO=tcp, DPORT=873 }
######################################################################################################
# IPSEC
#
?if __IPV4
DNAT		{ SOURCE=loc,net, DEST=apps:172.20.2.44, PROTO=udp, DPORT=500,4500, ORIGDEST=70.90.191.123 }
?else
ACCEPT		{ SOURCE=loc,net, DEST=apps, PROTO=udp, DPORT=500,4500 }
ACCEPT		{ SOURCE=loc,net, DEST=apps, PROTO=esp }
?endif
ACCEPT		{ SOURCE=$FW,     DEST=net,  PROTO=udp, SPORT=4500 }
######################################################################################################
# FIN & RST
RST(ACCEPT)	{ SOURCE=all, DEST=all }
FIN(ACCEPT)	{ SOURCE=all, DEST=all }
