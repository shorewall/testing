#!/bin/sh
#
# Compiled firewall script generated by Shorewall-perl
#
################################################################################
# Functions to execute the various user exits (extension scripts)
################################################################################

run_init_exit() {
    true
}

run_start_exit() {
    true
}

run_tcclear_exit() {
    true
}

run_started_exit() {
    true
}

run_stop_exit() {
    true
}

run_stopped_exit() {
    true
}

run_clear_exit() {
    true
}

run_refresh_exit() {
    true
}

run_refreshed_exit() {
    true
}

run_restored_exit() {
    true
}

run_isusable_exit() {
    true
}

run_findgw_exit() {
    true
}
################################################################################
# End user exit functions
################################################################################

#
# Setup Common Rules (/proc)
#
setup_common_rules() {
    
    progress_message2 Setting up ARP filtering...

    if [ -f /proc/sys/net/ipv4/conf/br0/arp_filter ]; then
	echo 1 > /proc/sys/net/ipv4/conf/br0/arp_filter
    fi

    if [ -f /proc/sys/net/ipv4/conf/br1/arp_filter ]; then
	echo 1 > /proc/sys/net/ipv4/conf/br1/arp_filter
    fi

    if [ -f /proc/sys/net/ipv4/conf/br2/arp_filter ]; then
	echo 1 > /proc/sys/net/ipv4/conf/br2/arp_filter
    fi

    if [ -f /proc/sys/net/ipv4/conf/br0/arp_ignore ]; then
	echo 2 > /proc/sys/net/ipv4/conf/br0/arp_ignore
    fi

    if [ -f /proc/sys/net/ipv4/conf/br1/arp_ignore ]; then
	echo 2 > /proc/sys/net/ipv4/conf/br1/arp_ignore
    fi

    if [ -f /proc/sys/net/ipv4/conf/br2/arp_ignore ]; then
	echo 2 > /proc/sys/net/ipv4/conf/br2/arp_ignore
    fi

    progress_message2 Setting up Route Filtering...

    for file in /proc/sys/net/ipv4/conf/*; do
	[ -f $file/rp_filter ] && echo 2 > $file/rp_filter
    done

    if [ -f /proc/sys/net/ipv4/conf/eth0/rp_filter ]; then
	echo 2 > /proc/sys/net/ipv4/conf/eth0/rp_filter
    else
	error_message "WARNING: Cannot set route filtering on eth0"
    fi

    echo 2 > /proc/sys/net/ipv4/conf/all/rp_filter
    echo 2 > /proc/sys/net/ipv4/conf/default/rp_filter
    [ -n "$g_noroutes" ] || $IP -4 route flush cache
    
    progress_message2 Setting up Martian Logging...

    for file in /proc/sys/net/ipv4/conf/*; do
	[ -f $file/log_martians ] && echo 1 > $file/log_martians
    done

    echo 0 > /proc/sys/net/ipv4/conf/all/log_martians

    progress_message2 Setting up Accept Source Routing...

    if [ -f /proc/sys/net/ipv4/conf/br0/accept_source_route ]; then
	echo 1 > /proc/sys/net/ipv4/conf/br0/accept_source_route
    fi

    if [ -f /proc/sys/net/ipv4/conf/br1/accept_source_route ]; then
	echo 1 > /proc/sys/net/ipv4/conf/br1/accept_source_route
    fi

    if [ -f /proc/sys/net/ipv4/conf/br2/accept_source_route ]; then
	echo 1 > /proc/sys/net/ipv4/conf/br2/accept_source_route
    fi

    progress_message2 Setting up Proxy ARP...

    if [ -f /proc/sys/net/ipv4/conf/br0/proxy_arp ] ; then
	echo 1 > /proc/sys/net/ipv4/conf/br0/proxy_arp
    fi

    if [ -f /proc/sys/net/ipv4/conf/br1/proxy_arp ] ; then
	echo 1 > /proc/sys/net/ipv4/conf/br1/proxy_arp
    fi

    if [ -f /proc/sys/net/ipv4/conf/lo/proxy_arp ] ; then
	echo 1 > /proc/sys/net/ipv4/conf/lo/proxy_arp
    fi

    if [ -f /proc/sys/net/ipv4/conf/br2/proxy_arp ] ; then
	echo 1 > /proc/sys/net/ipv4/conf/br2/proxy_arp
    fi

    #
    # Disable automatic helper association on kernel 3.5.0 and later
    #
    if [ -f /proc/sys/net/netfilter/nf_conntrack_helper ]; then
	progress_message "Disabling Kernel Automatic Helper Association"
	echo 0 > /proc/sys/net/netfilter/nf_conntrack_helper
    fi

    return 0
}

#
# Add Optional Interface br0 (-65535)
#
start_interface_br0() {
    if [ -n "$SW_BR0_IS_USABLE" ]; then
	> ${VARDIR}/undo_br0_routing

	cat <<EOF >> ${VARDIR}/undo_br0_routing
case \$COMMAND in
    enable|disable)
        ;;
    *)
        rm -f ${VARDIR}/br0.status
        ;;
esac
EOF
	if [ $COMMAND = enable ]; then
	    echo 1 > /proc/sys/net/ipv4/conf/br0/arp_filter
	    echo 2 > /proc/sys/net/ipv4/conf/br0/arp_ignore
	    echo 1 > /proc/sys/net/ipv4/conf/br0/accept_source_route
	fi

	echo 0 > ${VARDIR}/br0.status

	if [ $COMMAND = enable ]; then
	    rm -f ${VARDIR}/br0_disabled
	    progress_message2 "Optional interface br0 Started"
	fi

    else
	echo 1 > ${VARDIR}/br0.status
	error_message "WARNING: Optional Interface br0 is not usable -- br0 not Started"
    fi
} # End of start_interface_br0();

#
# Stop interface br0
#
stop_interface_br0() {
    if [ -f ${VARDIR}/undo_br0_routing ]; then
	. ${VARDIR}/undo_br0_routing
	rm -f ${VARDIR}/undo_br0_routing
	echo 1 > ${VARDIR}/br0.status
	progress_message2 "   Optional Interface br0 stopped"
    else
	startup_error "${VARDIR}/undo_br0_routing does not exist"
    fi
}

#
# Add Optional Interface br1 (-65534)
#
start_interface_br1() {
    if [ -n "$SW_BR1_IS_USABLE" ]; then
	> ${VARDIR}/undo_br1_routing

	cat <<EOF >> ${VARDIR}/undo_br1_routing
case \$COMMAND in
    enable|disable)
        ;;
    *)
        rm -f ${VARDIR}/br1.status
        ;;
esac
EOF
	if [ $COMMAND = enable ]; then
	    echo 1 > /proc/sys/net/ipv4/conf/br1/arp_filter
	    echo 2 > /proc/sys/net/ipv4/conf/br1/arp_ignore
	    echo 1 > /proc/sys/net/ipv4/conf/br1/accept_source_route
	fi

	echo 0 > ${VARDIR}/br1.status

	if [ $COMMAND = enable ]; then
	    rm -f ${VARDIR}/br1_disabled
	    progress_message2 "Optional interface br1 Started"

	    if [ -n "$g_forcereload" ]; then
		progress_message2 "The IP address or gateway of br1 has changed -- forcing reload of the ruleset"
		COMMAND=reload
		detect_configuration
		define_firewall
	    fi
	fi

	echo "$SW_BR1_GATEWAY" > ${VARDIR}/br1.gateway

    else
	echo 1 > ${VARDIR}/br1.status
	error_message "WARNING: Optional Interface br1 is not usable -- br1 not Started"
	
	echo "$SW_BR1_GATEWAY" > ${VARDIR}/br1.gateway
    fi
} # End of start_interface_br1();

#
# Stop interface br1
#
stop_interface_br1() {
    if [ -f ${VARDIR}/undo_br1_routing ]; then
	. ${VARDIR}/undo_br1_routing
	rm -f ${VARDIR}/undo_br1_routing
	echo 1 > ${VARDIR}/br1.status
	progress_message2 "   Optional Interface br1 stopped"
    else
	startup_error "${VARDIR}/undo_br1_routing does not exist"
    fi
}

#
# Add Optional Interface lo (-65533)
#
start_interface_lo() {
    if [ -n "$SW_LO_IS_USABLE" ]; then
	> ${VARDIR}/undo_lo_routing

	cat <<EOF >> ${VARDIR}/undo_lo_routing
case \$COMMAND in
    enable|disable)
        ;;
    *)
        rm -f ${VARDIR}/lo.status
        ;;
esac
EOF

	echo 0 > ${VARDIR}/lo.status

	if [ $COMMAND = enable ]; then
	    rm -f ${VARDIR}/lo_disabled
	    progress_message2 "Optional interface lo Started"
	fi

    else
	echo 1 > ${VARDIR}/lo.status
	error_message "WARNING: Optional Interface lo is not usable -- lo not Started"
    fi
} # End of start_interface_lo();

#
# Stop interface lo
#
stop_interface_lo() {
    if [ -f ${VARDIR}/undo_lo_routing ]; then
	. ${VARDIR}/undo_lo_routing
	rm -f ${VARDIR}/undo_lo_routing
	echo 1 > ${VARDIR}/lo.status
	progress_message2 "   Optional Interface lo stopped"
    else
	startup_error "${VARDIR}/undo_lo_routing does not exist"
    fi
}

#
# Add Optional Interface vif (-65532)
#
start_interface_vif() {
    if [ -n "$SW_PPP19_IS_USABLE" ]; then
	> ${VARDIR}/undo_vif_routing

	cat <<EOF >> ${VARDIR}/undo_vif_routing
case \$COMMAND in
    enable|disable)
        ;;
    *)
        rm -f ${VARDIR}/ppp19.status
        ;;
esac
EOF

	echo 0 > ${VARDIR}/ppp19.status

	if [ $COMMAND = enable ]; then
	    rm -f ${VARDIR}/ppp19_disabled
	    progress_message2 "Optional interface vif Started"

	    if [ -n "$g_forcereload" ]; then
		progress_message2 "The IP address or gateway of ppp19 has changed -- forcing reload of the ruleset"
		COMMAND=reload
		detect_configuration
		define_firewall
	    fi
	fi

	echo "$SW_VIF_GATEWAY" > ${VARDIR}/ppp19.gateway

    else
	echo 1 > ${VARDIR}/ppp19.status
	error_message "WARNING: Optional Interface ppp19 is not usable -- vif not Started"
	
	echo "$SW_VIF_GATEWAY" > ${VARDIR}/ppp19.gateway
    fi
} # End of start_interface_vif();

#
# Stop interface vif
#
stop_interface_vif() {
    if [ -f ${VARDIR}/undo_vif_routing 