#!/bin/sh -x

#qos script by TomVH

DEBUG=0

# To enable logging (requires iptables-mod-extra package)
[ $DEBUG -eq 1 ] && insmod ipt_LOG >&- 2>&-

#######################################################
DOWNLOAD=5000 #download speed in kbit. set xx% of real download speed
UPLOAD=600 # set xx% of real upload speed

# multiports = up to 15 ports
# ports to be classified as bulk #set after connection mark save and after connection mark restore
TCP_BULK="1024:" #S and D ports
UDP_BULK="1024:" #S and D ports

# Destination ports to be classified as P2P
TCP_P2P="13769" #D ports
UDP_P2P="13769" #D ports
IP_P2P="192.168.0.133"

# Destination ports to be classified as normal
TCP_NORMAL="80,443,25,20,21,110,993,995" # D ports
UDP_NORMAL=""

# Destination ports to be classified as Prio (overules bulk ports)
TCP_PRIO="22,53" #destination ports
UDP_PRIO="22,53" #destination ports

# Destination ports to be classified as VoIP (overules bulk ports)
TCP_VOIP=""
UDP_VOIP="18080"
IP_VOIP="192.168.0.226" #destination and source IP
IP_VOIP="192.168.0.226" #destination and source IP

#######################################################
iface="pppoe-wan"
#####################################################


#!!!!!uplink leaf class parameters!!!!!!!!!

#bulk
UP_LS_BULK_RATE=$(($UPLOAD*5/100))
UP_UL_BULK_RATE=$UPLOAD
#settings leaf qdisc
UP_BULK_RED_PROB=0.05 #red drob probability
UP_BULK_RED_min=6250 #real limit. To limit BULK traffic
UP_BULK_RED_min2=6250 #min for doing the calculations (burst and etc)
UP_BULK_RED_max=$((2 * $UP_BULK_RED_min2 + $UP_BULK_RED_min))
UP_BULK_RED_burst=$(((5 * $UP_BULK_RED_min2) / (3 * 1000)))
UP_BULK_RED_limit=$(($UP_BULK_RED_max * 5))

#P2P
UP_LS_P2P_RATE=$(($UPLOAD * 5 / 100))
UP_UL_P2P_RATE=$UPLOAD
#settings leaf qdisc
UP_P2P_RED_PROB=0.05 #red drob probability
UP_P2P_RED_min=32000 #real limit. To limit P2P traffic
UP_P2P_RED_min2=32000 #min for doing the calculations (burst and etc)
UP_P2P_RED_max=$((5 * $UP_P2P_RED_min2 + $UP_P2P_RED_min))
UP_P2P_RED_burst=$(((5 * $UP_P2P_RED_min2) / (3 * 1000)))
UP_P2P_RED_limit=$(($UP_P2P_RED_max * 5))
#normal class
UP_LS_NORMAL_RATE=$(($UPLOAD * 40 / 100))
UP_UL_NORMAL_RATE=$UPLOAD
#settings leaf qdisc
UP_NORMAL_RED_PROB=0.05 #red drob probability
UP_NORMAL_RED_min=6250 #real limit. To limit NORMAL traffic
UP_NORMAL_RED_min2=6250 #min for doing the calculations (burst and etc)
UP_NORMAL_RED_max=$((2 * $UP_NORMAL_RED_min2 + $UP_NORMAL_RED_min))
UP_NORMAL_RED_burst=$(((5 * $UP_NORMAL_RED_min2) / (3 * 1000)))
UP_NORMAL_RED_limit=$(($UP_NORMAL_RED_max * 5))

#prio
UP_LS_PRIO_RATE=$(($UPLOAD*50/100))
UP_RT_PRIO_RATE="200" #rate in kbit
UP_RT_PRIO_UMAX="400" #lengte of the packets [byte]
UP_RT_PRIO_DMAX="15" #delay in ms
UP_UL_PRIO_RATE=$UPLOAD

#Voip
UP_UL_VOIP_RATE=$UPLOAD
UP_SC_VOIP_RATE="200"
UP_SC_VOIP_UMAX="350" #length of the voip packets [byte]
UP_SC_VOIP_DMAX="10" #delay in ms



#!!!!!DOWNLIK leaf class parameters!!!!!!!!!

#bulk
DOWN_LS_BULK_RATE=$(($DOWNLOAD*5/100))
DOWN_UL_BULK_RATE=$DOWNLOAD
#leaf qdisc parameters
DOWN_BULK_RED_PROB=0.05 #red drob probability
DOWN_BULK_RED_min=62500 #real limit. To limit BULK traffic
DOWN_BULK_RED_min2=62500 #min for doing the calculations (burst and etc)
DOWN_BULK_RED_max=$((2 * $DOWN_BULK_RED_min2 + $DOWN_BULK_RED_min))
DOWN_BULK_RED_burst=$(((5 * $DOWN_BULK_RED_min2) / (3 * 1000)))
DOWN_BULK_RED_limit=$(($DOWN_BULK_RED_max * 5))


#P2P
DOWN_LS_P2P_RATE=$(($DOWNLOAD*5/100))
DOWN_UL_P2P_RATE=4000
#leaf qdisc parameters
DOWN_P2P_RED_PROB=0.05 #red drob probability
DOWN_P2P_RED_min=200000 #real limit. To limit P2P traffic
DOWN_P2P_RED_min2=200000 #min for doing the calculations (burst and etc)
DOWN_P2P_RED_max=$((2 * $DOWN_P2P_RED_min2 + $DOWN_P2P_RED_min))
DOWN_P2P_RED_burst=$(((5 * $DOWN_P2P_RED_min2) / (3 * 1000)))
DOWN_P2P_RED_limit=$(($DOWN_P2P_RED_max * 5))

#normal class
DOWN_LS_NORMAL_RATE=$(($DOWNLOAD*75/100))
DOWN_UL_NORMAL_RATE=$DOWNLOAD

#leaf qdisc parameters
DOWN_NORMAL_RED_PROB=0.05 #red drob probability
DOWN_NORMAL_RED_min=62500 #real limit. To limit NORMAL traffic
DOWN_NORMAL_RED_min2=62500 #min for doing the calculations (burst and etc)
DOWN_NORMAL_RED_max=$((2 * $DOWN_NORMAL_RED_min2 + $DOWN_NORMAL_RED_min))
DOWN_NORMAL_RED_burst=$(((5 * $DOWN_NORMAL_RED_min2) / (3 * 1000)))
DOWN_NORMAL_RED_limit=$(($DOWN_NORMAL_RED_max * 5))


#prio
DOWN_RT_PRIO_RATE="500" #rate in kbit
DOWN_RT_PRIO_UMAX="400" #length of the packets [byte]
DOWN_RT_PRIO_DMAX="1.5" #delay in ms
DOWN_UL_PRIO_RATE=$DOWNLOAD


#Voip
DOWN_UL_VOIP_RATE=$DOWNLOAD
DOWN_SC_VOIP_RATE="250"
DOWN_SC_VOIP_UMAX="350" #lengt of voip packets [byte]
DOWN_SC_VOIP_DMAX="1.2" #delay in ms


# The following packages are required for the modules:
# kmod-sched
# kmod-ipt-conntrack
# iptables-mod-conntrack
# kmod-ipt-ipopt
# iptables-mod-ipopt
# kmod-ipt-extra
# iptables-mod-extra

insmod ifb
insmod sch_hfsc
insmod sch_red
insmod sch_sfq
insmod cls_fw
insmod cls_u32
insmod em_u32
insmod act_connmark
insmod act_mirred
insmod sch_ingress

tc qdisc del dev $iface ingress
tc qdisc add dev $iface ingress
ifconfig ifb0 up txqueuelen 5
tc filter add dev $iface parent ffff: protocol ip prio 1 u32 match u32 0 0 flowid 1:1 action connmark action mirred egress redirect dev ifb0

#ecn isn't supported by default in win7 winXp,... So do not enable ECN of the uplaod qdiscs!!!
ifconfig $iface up txqueuelen 5
tc qdisc add dev $iface stab overhead 44 linklayer atm root handle 1: hfsc default 50 
tc class add dev $iface parent 1: classid 1:1 hfsc sc rate ${UPLOAD}kbit ul rate ${UPLOAD}kbit
tc class add dev $iface parent 1:1 classid 1:10 hfsc sc umax ${UP_SC_VOIP_UMAX}b dmax ${UP_SC_VOIP_DMAX}ms rate ${UP_SC_VOIP_RATE}kbit ul rate ${UP_UL_VOIP_RATE}kbit
tc class add dev $iface parent 1:1 classid 1:20 hfsc rt umax ${UP_RT_PRIO_UMAX}b dmax ${UP_RT_PRIO_DMAX}ms rate ${UP_RT_PRIO_RATE}kbit ls rate ${UP_LS_PRIO_RATE}kbit  ul rate ${UP_UL_PRIO_RATE}kbit
tc class add dev $iface parent 1:1 classid 1:30 hfsc ls rate ${UP_LS_NORMAL_RATE}kbit  ul rate ${UP_UL_NORMAL_RATE}kbit
tc class add dev $iface parent 1:1 classid 1:40 hfsc ls rate ${UP_LS_P2P_RATE}kbit  ul rate ${UP_UL_P2P_RATE}kbit
tc class add dev $iface parent 1:1 classid 1:50 hfsc ls rate ${UP_LS_BULK_RATE}kbit  ul rate ${UP_UL_BULK_RATE}kbit
tc qdisc add dev $iface parent 1:10 handle 100: sfq perturb 2 #limit xxx =>128 is the default limit
tc qdisc add dev $iface parent 1:20 handle 200: sfq perturb 2 #limit xxx =>128 is the default limit
tc qdisc add dev $iface parent 1:30 handle 300: red limit $UP_NORMAL_RED_limit min $UP_NORMAL_RED_min max $UP_NORMAL_RED_max avpkt 1000 burst $UP_NORMAL_RED_burst probability $UP_NORMAL_RED_PROB 
tc qdisc add dev $iface parent 1:40 handle 400: red limit $UP_P2P_RED_limit min $UP_P2P_RED_min max $UP_P2P_RED_max avpkt 1000 burst $UP_P2P_RED_burst probability $UP_P2P_RED_PROB
tc qdisc add dev $iface parent 1:50 handle 500: red limit $UP_BULK_RED_limit min $UP_BULK_RED_min max $UP_BULK_RED_max avpkt 1000 burst $UP_BULK_RED_burst probability $UP_BULK_RED_PROB 
tc filter add dev $iface parent 1: prio 1 protocol ip handle 1 fw flowid 1:10
tc filter add dev $iface parent 1: prio 2 protocol ip handle 2 fw flowid 1:20
tc filter add dev $iface parent 1: prio 3 protocol ip handle 3 fw flowid 1:30
tc filter add dev $iface parent 1: prio 4 protocol ip handle 4 fw flowid 1:40
tc filter add dev $iface parent 1: prio 5 protocol ip handle 5 fw flowid 1:50

tc qdisc add dev ifb0 stab overhead 44 linklayer atm root handle 1: hfsc default 50
tc class add dev ifb0 parent 1: classid 1:1 hfsc sc rate ${DOWNLOAD}kbit ul rate ${DOWNLOAD}kbit
tc class add dev ifb0 parent 1:1 classid 1:10 hfsc sc umax ${DOWN_SC_VOIP_UMAX}b dmax ${DOWN_SC_VOIP_DMAX}ms rate ${DOWN_SC_VOIP_RATE}kbit ul rate ${DOWN_UL_VOIP_RATE}kbit
tc class add dev ifb0 parent 1:1 classid 1:20 hfsc sc umax ${DOWN_RT_PRIO_UMAX}b dmax ${DOWN_RT_PRIO_DMAX}ms rate ${DOWN_RT_PRIO_RATE}kbit ul rate ${DOWN_UL_PRIO_RATE}kbit
tc class add dev ifb0 parent 1:1 classid 1:30 hfsc ls rate ${DOWN_LS_NORMAL_RATE}kbit  ul rate ${DOWN_UL_NORMAL_RATE}kbit
tc class add dev ifb0 parent 1:1 classid 1:40 hfsc ls rate ${DOWN_LS_P2P_RATE}kbit  ul rate ${DOWN_UL_P2P_RATE}kbit
tc class add dev ifb0 parent 1:1 classid 1:50 hfsc ls rate ${DOWN_LS_BULK_RATE}kbit  ul rate ${DOWN_UL_BULK_RATE}kbit
tc qdisc add dev ifb0 parent 1:10 handle 100: sfq perturb 2 #limit xxx =>128 is the default limit
tc qdisc add dev ifb0 parent 1:20 handle 200: sfq perturb 2 #limit xxx =>128 is the default limit
tc qdisc add dev ifb0 parent 1:30 handle 300: red limit $DOWN_NORMAL_RED_limit min $DOWN_NORMAL_RED_min max $DOWN_NORMAL_RED_max avpkt 1000 burst $DOWN_NORMAL_RED_burst probability $DOWN_NORMAL_RED_PROB
tc qdisc add dev ifb0 parent 1:40 handle 400: red limit $DOWN_P2P_RED_limit min $DOWN_P2P_RED_min max $DOWN_P2P_RED_max avpkt 1000 burst $DOWN_P2P_RED_burst probability $DOWN_P2P_RED_PROB
tc qdisc add dev ifb0 parent 1:50 handle 500: red limit $DOWN_BULK_RED_limit min $DOWN_BULK_RED_min max $DOWN_BULK_RED_max avpkt 1000 burst $DOWN_BULK_RED_burst probability $DOWN_BULK_RED_PROB
tc filter add dev ifb0 parent 1: prio 1 protocol ip handle 1 fw flowid 1:10
tc filter add dev ifb0 parent 1: prio 2 protocol ip handle 2 fw flowid 1:20
tc filter add dev ifb0 parent 1: prio 3 protocol ip handle 3 fw flowid 1:30
tc filter add dev ifb0 parent 1: prio 4 protocol ip handle 4 fw flowid 1:40
tc filter add dev ifb0 parent 1: prio 5 protocol ip handle 5 fw flowid 1:50



##########################
#####    IPTABLES    #####
##########################

iptables -t mangle -F QOS
iptables -t mangle -D FORWARD -o $iface -j QOS
iptables -t mangle -D OUTPUT -o $iface -j QOS
iptables -t mangle -X QOS

insmod ipt_layer7
insmod xt_layer7
insmod ipt_connbytes
insmod ipt_tos
insmod ipt_dscp
insmod ipt_length
insmod ipt_CONNMARK
insmod ipt_multiport

iptables -t mangle -N QOS
iptables -t mangle -A FORWARD -o $iface -j QOS
iptables -t mangle -A OUTPUT -o $iface -j QOS

#restore connection mark
iptables -t mangle -A QOS -j CONNMARK --restore-mark

#mark everything before connection mark store
#ICMP packages must by prio
iptables -t mangle -A QOS -s 192.168.0.145 -p icmp -j MARK --set-mark 2

#VoIP packages must be marked
iptables -t mangle -A QOS -p udp -m multiport --sports ${UDP_VOIP} -j MARK --set-mark 1
iptables -t mangle -A QOS -s ${IP_VOIP} -j MARK --set-mark 1
iptables -t mangle -A QOS -d ${IP_VOIP} -j MARK --set-mark 1
#may marked if not yet marked
iptables -t mangle -A QOS -m mark --mark 0 -p tcp -m multiport --dports ${TCP_PRIO} -j MARK --set-mark 2
iptables -t mangle -A QOS -m mark --mark 0 -p udp -m multiport --dports ${UDP_PRIO} -j MARK --set-mark 2
iptables -t mangle -A QOS -m mark --mark 0 -p tcp -m multiport --dports ${TCP_NORMAL} -j MARK --set-mark 3

#P2P traffic
iptables -t mangle -A QOS -s ${IP_P2P} -m mark --mark 0 -p tcp -m multiport --sports ${TCP_P2P} -j MARK --set-mark 4
iptables -t mangle -A QOS -s ${IP_P2P} -m mark --mark 0 -p udp -m multiport --sports ${UDP_P2P} -j MARK --set-mark 4

#bulk traffiek
iptables -t mangle -A QOS -m mark --mark 0 -p tcp --sport ${TCP_BULK} -j MARK --set-mark 5
iptables -t mangle -A QOS -m mark --mark 0 -p udp --sport ${UDP_BULK} -j MARK --set-mark 5
iptables -t mangle -A QOS -m mark --mark 0 -p tcp --dport ${TCP_BULK} -j MARK --set-mark 5
iptables -t mangle -A QOS -m mark --mark 0 -p udp --dport ${UDP_BULK} -j MARK --set-mark 5

#alles moet een mark 3 "normal" krijgen als ze nog niet gemarked zijn. Dit is de enige manier om utorrent te snoeren. By deafault moeten alle pakketen in de P2P class behalve als ze gemarked zijn.
iptables -t mangle -A QOS -m mark --mark 0 -j MARK --set-mark 3


# Save mark onto connection
iptables -t mangle -A QOS -j CONNMARK --save-mark

#reclasify any packets #zeker nog te bekijken welke flags we moeten zetten!!
#iptables -t mangle -A QOS -p tcp -m length --length :128 -m mark ! --mark 4 -m mark ! --mark 5 -m tcp --tcp-flags SYN,RST,ACK ACK -j MARK --set-mark 2

#debugging

[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -m mark --mark 0 -j LOG --log-prefix mark_0::
[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -m mark --mark 0 -j ACCEPT
[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -m mark --mark 1 -j LOG --log-prefix mark_1::
[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -m mark --mark 1 -j ACCEPT
[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -m mark --mark 2 -j LOG --log-prefix mark_2::
[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -m mark --mark 2 -j ACCEPT
[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -m mark --mark 3 -j LOG --log-prefix mark_3::
[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -m mark --mark 3 -j ACCEPT
[ $DEBUG -eq 1 ] && iptables -t mangle -A mark_chain -j LOG --log-prefix mark_other::

[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -m mark --mark 0 -j LOG --log-prefix ingress_0::
[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -m mark --mark 0 -j ACCEPT
[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -m mark --mark 1 -j LOG --log-prefix ingress_1::
[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -m mark --mark 1 -j ACCEPT
[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -m mark --mark 2 -j LOG --log-prefix ingress_2::
[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -m mark --mark 2 -j ACCEPT
[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -m mark --mark 3 -j LOG --log-prefix ingress_3::
[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -m mark --mark 3 -j ACCEPT
[ $DEBUG -eq 1 ] && iptables -t mangle -A ingress_chain -j LOG --log-prefix ingress_other::

Qdisc hierarchy
