#
# Shorewall version 4 - Drop Action
#
# /usr/share/shorewall/action.Drop
#
#	The default DROP common rules
#
#	This action is invoked before a DROP policy is enforced. The purpose
#	of the action is:
#
#	a) Avoid logging lots of useless cruft.
#	b) Ensure that 'auth' requests are rejected, even if the policy is
#	   DROP. Otherwise, you may experience problems establishing
#	   connections with servers that use auth.
#	c) Ensure that certain ICMP packets that are necessary for successful
#	   internet operation are always ACCEPTed.
#
#  The action accepts five optional parameters:
#
#	1 - 'audit' or '-'. Default is '-' which means don't audit in builtin
#           actions.
#       2 - Action to take with Auth requests. Default is REJECT or A_REJECT,
#           depending on the setting of the first parameter.
#	3 - Action to take with SMB requests. Default is DROP or A_DROP,
#           depending on the setting of the first parameter.
#       4 - Action to take with required ICMP packets. Default is ACCEPT or
#           A_ACCEPT depending on the first parameter.
#       5 - Action to take with late UDP replies (UDP source port 53). Default
#           is DROP or A_DROP depending on the first parameter.
#
# IF YOU ARE HAVING CONNECTION PROBLEMS, CHANGING THIS FILE WON'T HELP!!!!!!!!!
#
###############################################################################
?FORMAT 2
#TARGET		SOURCE	DEST	PROTO	DPORT	SPORT
#
# Count packets that come through here
#
COUNT
#
# Reject 'auth'
#
Auth(REJECT)
#
# Don't log broadcasts
#
Broadcast(DROP)
#
# ACCEPT critical ICMP types
#
AllowICMPs(ACCEPT)	-	-	icmp
#
# Drop packets that are in the INVALID state -- these are usually ICMP packets
# and just confuse people when they appear in the log.
#
Invalid(DROP)
#
# Drop Microsoft noise so that it doesn't clutter up the log.
#
SMB(DROP)
DropUPnP(DROP)
#
# Drop 'newnotsyn' traffic so that it doesn't get logged.
#
NotSyn(DROP)	-	-	tcp
#
# Drop late-arriving DNS replies. These are just a nuisance and clutter up
# the log.
#
DropDNSrep(DROP)
#
# Drop and log connections from selected countries.
#
?IF $GEOIP_MATCH
DROP:$LOG:China	 ^CN
DROP:$LOG:Russia ^RU
DROP:$LOG:Brazil ^BR
DROP:$LOG:Israel ^IL
DROP:$LOG:USA	 ^US
?ENDIF


