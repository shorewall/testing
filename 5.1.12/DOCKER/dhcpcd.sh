#!/bin/sh

if [ $2 != down ]; then
    if [ -f /var/lib/dhcpcd/dhcpcd-eth0.info ]; then
	. /var/lib/dhcpcd/dhcpcd-eth0.info
    else
	logger -p daemon.err "/var/lib/dhcpcd/dhcpcd-eth0.info does not exist!"
	exit 1
    fi

    logger -p daemon.info "DHCP-assigned address/gateway for eth0 is $IPADDR/$GATEWAYS"

    [ -f /var/lib/shorewall/eth0.info ] && . /var/lib/shorewall/eth0.info
    
    if [ "$GATEWAYS" != "$ETH0_GATEWAY" -o "$IPADDR" != "$ETH0_ADDRESS" ]; then
	logger -p daemon.info "eth0 IP configuration changed - restarting lsm and Shorewall"
	killall lsm
	/sbin/shorewall restart
    fi
fi

	
