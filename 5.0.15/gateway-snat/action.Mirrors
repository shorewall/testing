#
# Shorewall 1.4 /etc/shorewall/action.template
#
#	This file is a template for files with names of the form 
#	/etc/shorewall/action.<action-name> where <action-name> is an
#	TARGET defined in /etc/shorewall/actions.
#
#	To define a new action:
#
#	1. Add the <action name> to /etc/shorewall/actions
#	2. Copy this file to /etc/shorewall/action.<action name>
#	3. Add the desired rules to that file.
#
# Columns are:
#
#
#	TARGET		ACCEPT, DROP, REJECT, DNAT, DNAT-, REDIRECT, CONTINUE
#			or LOG.
#
#				ACCEPT   -- allow the connection request
#				DROP     -- ignore the request
#				REJECT   -- disallow the request and return an
#					    icmp-unreachable or an RST packet.
#				LOG      -- Simply log the packet and continue.
#				QUEUE	 -- Queue the packet to a user-space
#					    application such as p2pwall.
#
#			The TARGET may optionally be followed
#			by ":" and a syslog log level (e.g, REJECT:info or
#			DNAT:debugging). This causes the packet to be
#			logged at the specified level.
#
#			You may also specify ULOG (must be in upper case) as a
#			log level.This will log to the ULOG target for routing
#			to a separate log through use of ulogd
#			(http://www.gnumonks.org/projects/ulogd).
#
#	SOURCE		Source hosts to which the rule applies. 
#			A comma-separated list of subnets
#			and/or hosts. Hosts may be specified by IP or MAC
#			address; mac addresses must begin with "~" and must use
#			"-" as a separator.
#
#			192.168.2.2		Host 192.168.2.2
#
#			155.186.235.0/24	Subnet 155.186.235.0/24
#
#			192.168.1.1,192.168.1.2
#						Hosts 192.168.1.1 and
#						192.168.1.2.
#			~00-A0-C9-15-39-78      Host with
#                                               MAC address 00:A0:C9:15:39:78.
#
#			Alternatively, clients may be specified by interface
#			name. For example, eth1 specifies a
#			client that communicates with the firewall system
#			through eth1. This may be optionally followed by
#			another colon (":") and an IP/MAC/subnet address
#			as described above (e.g., eth1:192.168.1.5).
#
#	DEST		Location of Server. Same as above with the exception that
#			MAC addresses are not allowed.
#
#			Unlike in the SOURCE column, you may specify a range of
#			up to 256 IP addresses using the syntax
#			<first ip>-<last ip>.
#
#	PROTO		Protocol - Must be "tcp", "udp", "icmp", a number, or
#			"all".
#
#	DEST PORT(S)    Destination Ports. A comma-separated list of Port
#			names (from /etc/services), port numbers or port
#			ranges; if the protocol is "icmp", this column is
#			interpreted as the destination icmp-type(s).
#
#			A port range is expressed as <low port>:<high port>.
#
#			This column is ignored if PROTOCOL = all but must be
#			entered if any of the following ields are supplied.
#			In that case, it is suggested that this field contain
#			 "-"
#
#			If your kernel contains multi-port match support, then
#			only a single Netfilter rule will be generated if in
#			this list and the CLIENT PORT(S) list below:
#			1. There are 15 or less ports listed.
#			2. No port ranges are included.
#			Otherwise, a separate rule will be generated for each
#			port.
#
#	CLIENT PORT(S)	(Optional) Port(s) used by the client. If omitted,
#			any source port is acceptable. Specified as a comma-
#			separated list of port names, port numbers or port
#			ranges.
#
#			If you don't want to restrict client ports but need to
#			specify an ADDRESS in the next column, then place "-"
#			in this column.
#
#			If your kernel contains multi-port match support, then
#			only a single Netfilter rule will be generated if in
#			this list and the DEST PORT(S) list above:
#			1. There are 15 or less ports listed.
#			2. No port ranges are included.
#			Otherwise, a separate rule will be generated for each
#			port.
#
#	RATE LIMIT	You may rate-limit the rule by placing a value in 
#			this colume:
# 
#				<rate>/<interval>[:<burst>]
#
#			where <rate> is the number of connections per 
#			<interval> ("sec" or "min") and <burst> is the
#			largest burst permitted. If no <burst> is given,
#			a value of 5 is assumed. There may be no
#			no whitespace embedded in the specification.
#
#				Example: 10/sec:20
#
#			If you place a rate limit in this column, you may not
#			place a similar limit in the TARGET column.
#
######################################################################################
#TARGET SOURCE		DEST      	PROTO	DEST    SOURCE	   ORIGINAL	RATE
#                       	        	PORT    PORT(S)    DEST		LIMIT
?COMMENT Accept traffic from Mirrors
ACCEPT	$MIRRORS
#LAST LINE -- ADD YOUR ENTRIES BEFORE THIS ONE -- DO NOT REMOVE
