#!/bin/sh
#
# Compiled firewall script generated by Shorewall-perl
#
################################################################################
# Functions to execute the various user exits (extension scripts)
################################################################################

run_init_exit() {
    true
}

run_start_exit() {
    true
}

run_tcclear_exit() {
    true
}

run_started_exit() {
    true
}

run_stop_exit() {
    true
}

run_stopped_exit() {
    true
}

run_clear_exit() {
    true
}

run_refresh_exit() {
    true
}

run_refreshed_exit() {
    true
}

run_restored_exit() {
    true
}

run_isusable_exit() {
    true
}

run_findgw_exit() {
    true
}
################################################################################
# End user exit functions
################################################################################

#
# Setup Common Rules (/proc)
#
setup_common_rules() {
    
    progress_message2 Setting up Route Filtering...

    for file in /proc/sys/net/ipv4/conf/*; do
	[ -f $file/rp_filter ] && echo 0 > $file/rp_filter
    done

    echo 1 > /proc/sys/net/ipv4/conf/all/rp_filter
    echo 0 > /proc/sys/net/ipv4/conf/default/rp_filter
    [ -n "$g_noroutes" ] || $IP -4 route flush cache
    
    progress_message2 Setting up Martian Logging...

    for file in /proc/sys/net/ipv4/conf/*; do
	[ -f $file/log_martians ] && echo 1 > $file/log_martians
    done

    echo 0 > /proc/sys/net/ipv4/conf/all/log_martians

    if [ -f /proc/sys/net/ipv4/conf/eth0/log_martians ]; then
	echo 1 > /proc/sys/net/ipv4/conf/eth0/log_martians
    else
	error_message "WARNING: Cannot set Martian logging on eth0"
    fi

    #
    # Disable automatic helper association on kernel 3.5.0 and later
    #
    if [ -f /proc/sys/net/netfilter/nf_conntrack_helper ]; then
	progress_message "Disabling Kernel Automatic Helper Association"
	echo 0 > /proc/sys/net/netfilter/nf_conntrack_helper
    fi

    return 0
}

#
# Configure Traffic Shaping for eth0
#
setup_eth0_tc() {
    if interface_is_up eth0; then
	qt $TC qdisc del dev eth0 root
	qt $TC qdisc del dev eth0 ingress
	run_tc qdisc add dev eth0 root handle a: hfsc default 11
	run_tc class add dev eth0 parent a: classid a:1 hfsc sc rate 1000kbit ul rate 1000kbit
	run_tc class add dev eth0 parent a:1 classid a:11 hfsc sc umax $(get_device_mtu eth0)b dmax 50ms rate 100kbit ul rate 200kbit
	run_tc qdisc add dev eth0 parent a:11 handle 1: sfq limit 127 perturb 10
	run_tc filter add dev eth0 parent a:0 protocol ip prio 266 u32\
	    match ip protocol 6 0xff\
	    match u8 0x05 0x0f at 0\
	    match u16 0x0000 0xffc0 at 2\
	    match u8 0x10 0xff at 33 flowid a:11
	progress_message "   TC Class a:11 defined."

	progress_message "   TC Device eth0 defined."
    else
	error_message "WARNING: Device eth0 is not in the UP state -- traffic-shaping configuration skipped"
    fi

}

#
# Configure Traffic Shaping for ifb0
#
setup_ifb0_tc() {
    if interface_is_up ifb0; then
	qt $TC qdisc del dev ifb0 root
	qt $TC qdisc del dev ifb0 ingress
	ifb0_mtu=$(get_device_mtu ifb0)
	ifb0_mtu1=$(get_device_mtu1 ifb0)
	run_tc qdisc add dev ifb0 root handle b: htb default 21 r2q 5
	run_tc class add dev ifb0 parent b: classid b:1 htb rate 1000kbit $ifb0_mtu1
	run_tc qdisc add dev eth0 handle ffff: ingress
	run_tc filter add dev eth0 parent ffff: protocol all u32 match u32 0 0 action mirred egress redirect dev ifb0 > /dev/null
	[ $ifb0_mtu -gt 12500 ] && quantum=$ifb0_mtu || quantum=12500
	run_tc class add dev ifb0 parent b:1 classid b:21 htb rate 500kbit ceil 1000kbit prio 1 $ifb0_mtu1 quantum $quantum
	run_tc qdisc add dev ifb0 parent b:21 handle 2: sfq quantum $quantum limit 127 perturb 10
	progress_message "   TC Class b:21 defined."

	progress_message "   TC Device ifb0 defined."
    else
	error_message "WARNING: Device ifb0 is not in the UP state -- traffic-shaping configuration skipped"
    fi

}

#
# Enable an optional provider
#
enable_provider() {
    g_interface=$1;

    case $g_interface in

	*)
	    startup_error "$g_interface is not an optional provider or interface"
	    ;;
    esac

}

#
# Disable an optional provider
#
disable_provider() {
    g_interface=$1;

    case $g_interface in

	*)
	    startup_error "$g_interface is not an optional provider interface"
	    ;;
    esac
}

#
# Setup routing and traffic shaping
#
setup_routing_and_traffic_shaping() {
    
    if [ -z "$g_noroutes" ]; then
	
	undo_routing
	restore_default_route 
    fi

    progress_message2 "Setting up Traffic Control..."

    setup_eth0_tc
    setup_ifb0_tc
}

#
# This function initializes the global variables used by the program
#
initialize()
{
    #
    # Be sure that umask is sane
    #
    umask 077

    #
    # These variables are required by the library functions called in this script
    #
    g_family=4
    g_confdir=/etc/shorewall
    g_product=Shorewall
    g_program=shorewall
    g_basedir=/usr/share/shorewall
    CONFIG_PATH="/home/user/shorewall/regressionLibrary/5.0.15/ifb2-mangle/:/usr/share/shorewall/:/usr/share/shorewall/deprecated/"
    [ -f ${g_confdir}/vardir ] && . ${g_confdir}/vardir
    [ -n "${VARDIR:=/var/lib/shorewall}" ]
    [ -n "${VARLIB:=/var/lib}" ]
    TEMPFILE=
    DISABLE_IPV6=""
    MODULESDIR=""
    MODULE_SUFFIX="ko"
    LOAD_HELPERS_ONLY="Yes"
    LOCKFILE=""
    SUBSYSLOCK=""
    LOG_VERBOSITY="2"
    RESTART="reload"
    [ -n "${COMMAND:=reload}" ]
    [ -n "${VERBOSITY:=0}" ]
    [ -n "${RESTOREFILE:=restore}" ]
    PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin:/usr/local/sbin"
    TERMINATOR=fatal_error
    DONT_LOAD=""
    STARTUP_LOG="/var/log/shorewall-init.log"

    [ -z "$IPTABLES" ] && IPTABLES=$(mywhich iptables) # /sbin/shorewall exports IPTABLES
    [ -n "$IPTABLES" -a -x "$IPTABLES" ] || startup_error "Can't find iptables executable"

    case $IPTABLES in
	*/*)
	    ;;
	*)
	    IPTABLES=./$IPTABLES
	    ;;
    esac

    IP6TABLES=${IPTABLES%/*}/ip6tables
    IPTABLES_RESTORE=${IPTABLES}-restore
    [ -x "$IPTABLES_RESTORE" ] || startup_error "$IPTABLES_RESTORE does not exist or is not executable"
    g_tool=$IPTABLES
    IP=ip
    TC=tc
    IPSET=ipset

    g_stopping=

    #
    # The library requires that ${VARDIR} exist
    #
    [ -d ${VARDIR} ] || mkdir -p ${VARDIR}

}

#
# Set global variables holding detected IP information
#
detect_configuration()
{
    case $COMMAND in
	restore)
	    ;;
	*)
	    ALL_BCASTS="$(get_all_bcasts) 255.255.255.255"
	    ;;
    esac

}

#
# Create the input to iptables-restore/ip6tables-restore and pass that input to the utility
#
setup_netfilter()
{
    local option

    if [ "$COMMAND" = reload -a -n "$g_counters" ] && chain_exists $g_sha1sum1 && chain_exists $g_sha1sum2 ; then
	option="--counters"

	progress_message "Reusing existing ruleset..."

    else
	option=
	
	progress_message2 Preparing iptables-restore input...

	exec 3>${VARDIR}/.iptables-restore-input

	cat >&3 << __EOF__
*raw
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
COMMIT
*mangle
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
-A FORWARD -j MARK --set-mark 0/0xff
-A POSTROUTING -s 10.1.10.0/24 -o eth0 -j CLASSIFY --set-class a:11
COMMIT
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT DROP [0:0]
:Broadcast - [0:0]
:Drop - [0:0]
:Reject - [0:0]
:dynamic - [0:0]
:fw2net - [0:0]
:logdrop - [0:0]
:logflags - [0:0]
:logreject - [0:0]
:net2fw - [0:0]
:reject - [0:0]
:smurflog - [0:0]
:smurfs - [0:0]
:tcpflags - [0:0]
:~log0 - [0:0]
:$g_sha1sum1 - [0:0]
:$g_sha1sum2 - [0:0]
-A INPUT -i eth0 -j net2fw
-A INPUT -i lo -j ACCEPT
-A INPUT -j Reject
-A INPUT -j LOG --log-level 6 --log-prefix "Shorewall:INPUT:REJECT:"
-A INPUT -g reject
-A FORWARD -j Reject
-A FORWARD -j LOG --log-level 6 --log-prefix "Shorewall:FORWARD:REJECT:"
-A FORWARD -g reject
-A OUTPUT -o eth0 -j fw2net
-A OUTPUT -o lo -j ACCEPT
-A OUTPUT -j Reject
-A OUTPUT -j LOG --log-level 6 --log-prefix "Shorewall:OUTPUT:REJECT:"
-A OUTPUT -g reject
__EOF__

	for address in $ALL_BCASTS; do
	    echo "-A Broadcast -d $address -j DROP" >&3
	done

	cat >&3 << __EOF__
-A Broadcast -d 224.0.0.0/4 -j DROP
-A Drop
-A Drop -p 1 --icmp-type 3/4 -j ACCEPT -m comment --comment "Needed ICMP types"
-A Drop -p 1 --icmp-type 11 -j ACCEPT -m comment --comment "Needed ICMP types"
-A Drop -j Broadcast
-A Drop -m conntrack --ctstate INVALID -j DROP
-A Drop -p 17 -m multiport --dports 135,445 -j DROP -m comment --comment "SMB"
-A Drop -p 17 --dport 137:139 -j DROP -m comment --comment "SMB"
-A Drop -p 17 --dport 1024:65535 --sport 137 -j DROP -m comment --comment "SMB"
-A Drop -p 6 -m multiport --dports 135,139,445 -j DROP -m comment --comment "SMB"
-A Drop -p 17 --dport 1900 -j DROP -m comment --comment "UPnP"
-A Drop -p 6 ! --syn -j DROP
-A Drop -p 17 --sport 53 -j DROP -m comment --comment "Late DNS Replies"
-A Reject
-A Reject -p 1 --icmp-type 3/4 -j ACCEPT -m comment --comment "Needed ICMP types"
-A Reject -p 1 --icmp-type 11 -j ACCEPT -m comment --comment "Needed ICMP types"
-A Reject -j Broadcast
-A Reject -m conntrack --ctstate INVALID -j DROP
-A Reject -p 17 -m multiport --dports 135,445 -g reject -m comment --comment "SMB"
-A Reject -p 17 --dport 137:139 -g reject -m comment --comment "SMB"
-A Reject -p 17 --dport 1024:65535 --sport 137 -g reject -m comment --comment "SMB"
-A Reject -p 6 -m multiport --dports 135,139,445 -g reject -m comment --comment "SMB"
-A Reject -p 17 --dport 1900 -j DROP -m comment --comment "UPnP"
-A Reject -p 6 ! --syn -j DROP
-A Reject -p 17 --sport 53 -j DROP -m comment --comment "Late DNS Replies"
__EOF__

	[ -f ${VARDIR}/.dynamic ] && cat ${VARDIR}/.dynamic >&3

	cat >&3 << __EOF__
-A fw2net -p udp --dport 67:68 -j ACCEPT
-A fw2net -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A fw2net -p 1 -j ACCEPT
-A fw2net -p 17 --dport 67:68 --sport 67:68 -j DROP -m comment --comment "DHCPfwd"
-A fw2net -j ACCEPT
-A logdrop -j DROP
-A logflags -j LOG --log-ip-options --log-level 6 --log-prefix "Shorewall:logflags:DROP:"
-A logflags -j DROP
-A logreject -j reject
-A net2fw -m conntrack --ctstate NEW,INVALID,UNTRACKED -j dynamic
-A net2fw -m conntrack --ctstate NEW,INVALID,UNTRACKED -j smurfs
-A net2fw -p udp --dport 67:68 -j ACCEPT
-A net2fw -p tcp -j tcpflags
-A net2fw -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A net2fw -p 1 --icmp-type 8 -j DROP -m comment --comment "Ping"
-A net2fw -p 17 --dport 67:68 --sport 67:68 -j DROP -m comment --comment "DHCPfwd"
-A net2fw -p 6 --dport 9999 -g ~log0
-A net2fw -j Drop
-A net2fw -j LOG --log-level 6 --log-prefix "Shorewall:net2fw:DROP:"
-A net2fw -j DROP
__EOF__

	for address in $ALL_BCASTS; do
	    echo "-A reject -d $address -j DROP" >&3
	done

	cat >&3 << __EOF__
-A reject -s 224.0.0.0/4 -j DROP
-A reject -p 2 -j DROP
-A reject -p 6 -j REJECT --reject-with tcp-reset
-A reject -p 17 -j REJECT
-A reject -p 1 -j REJECT --reject-with icmp-host-unreachable
-A reject -j REJECT --reject-with icmp-host-prohibited
-A smurflog -j LOG --log-level 6 --log-prefix "Shorewall:smurfs:DROP:"
-A smurflog -j DROP
__EOF__

	for address in $ALL_BCASTS; do
	    echo "-A smurfs -s $address -g smurflog" >&3
	done

	cat >&3 << __EOF__
-A smurfs -s 224.0.0.0/4 -g smurflog
-A tcpflags -p tcp --tcp-flags ALL FIN,URG,PSH -g logflags
-A tcpflags -p tcp --tcp-flags ALL NONE -g logflags
-A tcpflags -p tcp --tcp-flags SYN,RST SYN,RST -g logflags
-A tcpflags -p tcp --tcp-flags FIN,RST FIN,RST -g logflags
-A tcpflags -p tcp --tcp-flags SYN,FIN SYN,FIN -g logflags
-A tcpflags -p tcp --tcp-flags ACK,PSH,FIN PSH,FIN -g logflags
-A tcpflags -p tcp --syn --sport 0 -g logflags
-A ~log0 -j LOG --log-level 6 --log-prefix "Shorewall:net2fw:ACCEPT:"
-A ~log0 -j ACCEPT
COMMIT
__EOF__

    fi

    exec 3>&-

    [ -n "$g_debug_iptables" ] && command=debug_restore_input || command="$IPTABLES_RESTORE $option"

    progress_message2 "Running $command..."

    cat ${VARDIR}/.iptables-restore-input | $command # Use this nonsensical form to appease SELinux
    if [ $? != 0 ]; then
	fatal_error "iptables-restore Failed. Input is in ${VARDIR}/.iptables-restore-input"
    fi

}

chainlist_reload()
{
    true
}

#
#Save the ipsets specified by the SAVE_IPSETS setting and by dynamic zones and blacklisting
#
save_ipsets() {
    true
}

#
#Flush and Destroy the sets then load fresh copy from a saved ipset file
#
load_ipsets() {
    true
}
#
# Start/Reload the Firewall
#
define_firewall() {
    local options
    
    progress_message2 Initializing...

    load_kernel_modules Yes

    if [ "$COMMAND" = refresh ]; then
       run_refresh_exit
    else
	run_init_exit
    fi

    load_ipsets

    if [ "$COMMAND" = reload -o "$COMMAND" = refresh ]; then
	if [ -n "$g_counters" ]; then
	    ${IPTABLES}-save --counters | grep -vE '[ :]shorewall ' > ${VARDIR}/.iptables-restore-input
	fi

	if chain_exists 'UPnP -t nat'; then
	    iptables-restore -t nat | grep '^-A UPnP ' > ${VARDIR}/.UPnP
	else
	    rm -f ${VARDIR}/.UPnP
	fi
	
	if chain_exists forwardUPnP; then
	    iptables-restore -t filter | grep '^-A forwardUPnP ' > ${VARDIR}/.forwardUPnP
	else
	    rm -f ${VARDIR}/.forwardUPnP
	fi
	
	if chain_exists dynamic; then
	    iptables-restore -t filter | grep '^-A dynamic ' > ${VARDIR}/.dynamic
	else
	    rm -f ${VARDIR}/.dynamic
	fi

    else
	rm -f ${VARDIR}/.UPnP
	rm -f ${VARDIR}/.forwardUPnP

    fi

    qt1 $IPTABLES -L shorewall -n && qt1 $IPTABLES -F shorewall && qt1 $IPTABLES -X shorewall

    delete_proxyarp

    delete_tc1

    setup_common_rules

    setup_routing_and_traffic_shaping

    cat > ${VARDIR}/proxyarp << __EOF__
__EOF__

    if [ "$COMMAND" != refresh ]; then
	cat > ${VARDIR}/zones << __EOF__
fw firewall
net ipv4 eth0:0.0.0.0/0
__EOF__
	cat > ${VARDIR}/policies << __EOF__
fw 	=>	net	ACCEPT using chain fw2net
net 	=>	fw	DROP using chain net2fw
__EOF__
	cat > ${VARDIR}/marks << __EOF__
Traffic Shaping:0-16383 (0x0-0x3fff) mask 0xffff
User: Not Enabled
Provider:1-255 (0x1-0xff) mask 0xff
Zone: Not Enabled
Exclusion:65536 mask 0x10000
TProxy:131072 mask 0x20000
__EOF__
    fi

    > ${VARDIR}/nat

    if [ $COMMAND = restore ]; then
	iptables_save_file=${VARDIR}/$(basename $0)-iptables
	if [ -f $iptables_save_file ]; then
	    [ -n "$g_counters" ] && options=--counters
	    cat $iptables_save_file | $IPTABLES_RESTORE $options # Use this nonsensical form to appease SELinux
	else
	   fatal_error "$iptables_save_file does not exist"
	fi

	echo 0 > /proc/sys/net/ipv4/ip_forward
	progress_message2 IPv4 Forwarding Disabled!

	set_state Started /home/user/shorewall/regressionLibrary/5.0.15/ifb2-mangle/
	run_restored_exit
    elif [ $COMMAND = refresh ]; then
	chainlist_reload

	echo 0 > /proc/sys/net/ipv4/ip_forward
	progress_message2 IPv4 Forwarding Disabled!

	run_refreshed_exit
	do_iptables -N shorewall
	set_state Started /home/user/shorewall/regressionLibrary/5.0.15/ifb2-mangle/
	[ $0 = ${VARDIR}/firewall ] || cp -f $(my_pathname) ${VARDIR}/firewall
    else
	setup_netfilter
	conditionally_flush_conntrack

	echo 0 > /proc/sys/net/ipv4/ip_forward
	progress_message2 IPv4 Forwarding Disabled!

	run_start_exit
	do_iptables -N shorewall

	set_state Started /home/user/shorewall/regressionLibrary/5.0.15/ifb2-mangle/
	my_pathname=$(my_pathname)
	[ $my_pathname = ${VARDIR}/firewall ] || cp -f $my_pathname ${VARDIR}/firewall
	run_started_exit
    fi

    date > ${VARDIR}/restarted
    
    case $COMMAND in
	start)
	    mylogger kern.info "$g_product started"
	    ;;
	reload)
	    mylogger kern.info "$g_product reloaded"
	    ;;
	refresh)
	    mylogger kern.info "$g_product refreshed"
	    ;;
	restore)
	    mylogger kern.info "$g_product restored"
	    ;;
    esac

}

#
# Stop/restore the firewall after an error or because of a 'stop' or 'clear' command
#
stop_firewall() {

    deletechain() {
	qt $IPTABLES -L $1 -n && qt $IPTABLES -F $1 && qt $IPTABLES -X $1
    }

    case $COMMAND in
	stop|clear|restore)
	    if chain_exists dynamic; then
		${IPTABLES}-save -t filter | grep '^-A dynamic' | fgrep -v -- '-j ACCEPT' > ${VARDIR}/.dynamic
	    fi
	    ;;
	*)
	    set +x

	    case $COMMAND in
		start)
		    mylogger kern.err "ERROR:$g_product start failed"
		    ;;
		reload)
		    mylogger kern.err "ERROR:$g_product reload failed"
		    ;;
		refresh)
		    mylogger kern.err "ERROR:$g_product refresh failed"
		    ;;
		enable)
		    mylogger kern.err "ERROR:$g_product 'enable $g_interface' failed"
		    ;;
	    esac

	    if [ "$RESTOREFILE" = NONE ]; then
		COMMAND=clear
		clear_firewall
		echo "$g_product Cleared"

		kill $$
		exit 2
	    else
		g_restorepath=${VARDIR}/$RESTOREFILE

		if [ -x $g_restorepath ]; then
		    echo Restoring ${g_product:=Shorewall}...

		    g_recovering=Yes

		    if run_it $g_restorepath restore; then
			echo "$g_product restored from $g_restorepath"
			set_state "Restored from $g_restorepath"
		    else
			set_state "Unknown"
		    fi

		    kill $$
		    exit 2
		fi
	    fi
	    ;;
    esac

    if [ -n "$g_stopping" ]; then
	kill $$
	exit 1
    fi

    set_state "Stopping"

    g_stopping="Yes"

    deletechain shorewall

    run_stop_exit

    #
    # Enable automatic helper association on kernel 3.5.0 and later
    #
    if [ $COMMAND = clear -a -f /proc/sys/net/netfilter/nf_conntrack_helper ]; then
	echo 1 > /proc/sys/net/netfilter/nf_conntrack_helper
    fi

    if [ -f ${VARDIR}/proxyarp ]; then
	while read address interface external haveroute; do
	    qtnoin $IP -4 neigh del proxy $address dev $external
	    [ -z "${haveroute}${g_noroutes}" ] && qtnoin $IP -4 route del $address/32 dev $interface
	    f=/proc/sys/net/ipv4/conf/$interface/proxy_arp
	    [ -f $f ] && echo 0 > $f
	done < ${VARDIR}/proxyarp

	rm -f ${VARDIR}/proxyarp
    fi


    delete_tc1
    undo_routing
    restore_default_route 

    progress_message2 Preparing iptables-restore input...

    exec 3>${VARDIR}/.iptables-restore-stop-input

    cat >&3 << __EOF__
*raw
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
COMMIT
*mangle
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
COMMIT
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -p udp --dport 67:68 -i eth0 -j ACCEPT
-A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A FORWARD -p udp --dport 67:68 -i eth0 -o eth0 -j ACCEPT
COMMIT
__EOF__

    [ -n "$g_debug_iptables" ] && command=debug_restore_input || command=$IPTABLES_RESTORE

    progress_message2 "Running $command..."

    cat ${VARDIR}/.iptables-restore-stop-input | $command # Use this nonsensical form to appease SELinux

    if [ $? != 0 ]; then
	error_message "ERROR: $command Failed."
    fi

    echo 0 > /proc/sys/net/ipv4/ip_forward
    progress_message2 IPv4 Forwarding Disabled!

    rm -f ${VARDIR}/*.address
    rm -f ${VARDIR}/*.gateway

    run_stopped_exit


    set_state "Stopped"
    mylogger kern.info "$g_product Stopped"

    case $COMMAND in
    stop|clear)
	;;
    *)
	#
	# The firewall is being stopped when we were trying to do something
	# else. Kill the shell in case we're running in a subshell
	#
	kill $$
	;;
    esac
}

#
# Handle the "up" and "down" commands
#
updown() # $1 = interface
{
    local state
    state=cleared

    progress_message3 "$g_product $COMMAND triggered by $1"

    if shorewall_is_started; then
	state=started
    elif [ -f ${VARDIR}/state ]; then
	case "$(cat ${VARDIR}/state)" in
	    Stopped*)
		state=stopped
		;;
	    Cleared*)
		;;
	    *)
		state=unknown
		;;
	esac
    else
	state=unknown
    fi

    case $1 in
	lo)
	    progress_message3 "$COMMAND on interface $1 ignored"
	    exit 0
	    ;;
	eth0)
	    case $state in
		started)
		    COMMAND=reload
		    progress_message3 "$g_product attempting reload"
		    detect_configuration
		    define_firewall
		    ;;
		*)
		    progress_message3 "$COMMAND on interface $1 ignored"
		    ;;
	    esac
    esac
}

