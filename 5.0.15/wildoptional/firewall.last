#!/bin/sh
#
# Compiled firewall script generated by Shorewall-perl
#
################################################################################
# Functions to execute the various user exits (extension scripts)
################################################################################

run_init_exit() {
    true
}

run_start_exit() {
    true
}

run_tcclear_exit() {
    true
}

run_started_exit() {
    true
}

run_stop_exit() {
    true
}

run_stopped_exit() {
    true
}

run_clear_exit() {
    true
}

run_refresh_exit() {
    true
}

run_refreshed_exit() {
    true
}

run_restored_exit() {
    true
}

run_isusable_exit() {
    true
}

run_findgw_exit() {
    true
}
################################################################################
# End user exit functions
################################################################################

#
# Setup Common Rules (/proc)
#
setup_common_rules() {
    
    progress_message2 Setting up Route Filtering...

    for file in /proc/sys/net/ipv4/conf/*; do
	[ -f $file/rp_filter ] && echo 0 > $file/rp_filter
    done

    if [ -f /proc/sys/net/ipv4/conf/eth0/rp_filter ]; then
	echo 1 > /proc/sys/net/ipv4/conf/eth0/rp_filter
    else
	error_message "WARNING: Cannot set route filtering on eth0"
    fi

    echo 0 > /proc/sys/net/ipv4/conf/all/rp_filter
    echo 0 > /proc/sys/net/ipv4/conf/default/rp_filter
    [ -n "$g_noroutes" ] || $IP -4 route flush cache
    
    progress_message2 Setting up Martian Logging...

    for file in /proc/sys/net/ipv4/conf/*; do
	[ -f $file/log_martians ] && echo 1 > $file/log_martians
    done

    echo 0 > /proc/sys/net/ipv4/conf/all/log_martians

    if [ -f /proc/sys/net/ipv4/conf/eth0/log_martians ]; then
	echo 1 > /proc/sys/net/ipv4/conf/eth0/log_martians
    else
	error_message "WARNING: Cannot set Martian logging on eth0"
    fi

    #
    # Disable automatic helper association on kernel 3.5.0 and later
    #
    if [ -f /proc/sys/net/netfilter/nf_conntrack_helper ]; then
	progress_message "Disabling Kernel Automatic Helper Association"
	echo 0 > /proc/sys/net/netfilter/nf_conntrack_helper
    fi

    return 0
}

#
# Add Provider wild (1)
#
start_provider_wild() {
    if [ -n "$SW_PPP0_IS_USABLE" ]; then
	qt ip -4 route flush table 1
	echo "$IP -4 route flush table 1 > /dev/null 2>&1" > ${VARDIR}/undo_wild_routing
	run_ip route add default dev ppp0 table 1

	cat <<EOF >> ${VARDIR}/undo_wild_routing
case \$COMMAND in
    enable|disable)
        ;;
    *)
        rm -f ${VARDIR}/ppp0.status
        ;;
esac
EOF
	qt $IP -4 rule del fwmark 0x1/0xff
	run_ip rule add fwmark 0x1/0xff pref 10000 table 1
	echo "$IP -4 rule del fwmark 0x1/0xff > /dev/null 2>&1" >> ${VARDIR}/undo_wild_routing

	find_interface_addresses ppp0 | while read address; do
	    qt $IP -4 rule del from $address
	    run_ip rule add from $address pref 20000 table 1
	    echo "$IP -4 rule del from $address pref 20000 > /dev/null 2>&1" >> ${VARDIR}/undo_wild_routing
	    rulenum=$(($rulenum + 1))
	done

	echo 0 > ${VARDIR}/ppp0.status

	if [ $COMMAND = enable ]; then
	    rm -f ${VARDIR}/ppp0_disabled
	    progress_message2 "Provider wild (1) Started"
	else
	    echo 1 > ${VARDIR}/ppp0_weight
	    progress_message "Provider wild (1) Started"
	fi

    else
	echo 1 > ${VARDIR}/ppp0.status
	error_message "WARNING: Interface ppp0 is not usable -- Provider wild (1) not Started"
    fi
} # End of start_provider_wild();

#
# Stop provider wild
#
stop_provider_wild() {
    if [ -f ${VARDIR}/undo_wild_routing ]; then
	. ${VARDIR}/undo_wild_routing
	> ${VARDIR}/undo_wild_routing
	echo 1 > ${VARDIR}/ppp0.status
	progress_message2 "   Provider wild (1) stopped"
    else
	startup_error "${VARDIR}/undo_wild_routing does not exist"
    fi
}

#
# Add Provider std (2)
#
start_provider_std() {
    if [ -n "$SW_ETH1_IS_USABLE" ]; then
	qt ip -4 route flush table 2
	echo "$IP -4 route flush table 2 > /dev/null 2>&1" > ${VARDIR}/undo_std_routing

	cat <<EOF >> ${VARDIR}/undo_std_routing
case \$COMMAND in
    enable|disable)
        ;;
    *)
        rm -f ${VARDIR}/eth1.status
        ;;
esac
EOF
	qt $IP -4 rule del fwmark 0x2/0xff
	run_ip rule add fwmark 0x2/0xff pref 10001 table 2
	echo "$IP -4 rule del fwmark 0x2/0xff > /dev/null 2>&1" >> ${VARDIR}/undo_std_routing
	run_ip route replace $SW_ETH1_GATEWAY src $SW_ETH1_ADDRESS dev eth1 
	run_ip route replace $SW_ETH1_GATEWAY src $SW_ETH1_ADDRESS dev eth1 table 2 
	run_ip route add default via $SW_ETH1_GATEWAY src $SW_ETH1_ADDRESS dev eth1 table 2 

	find_interface_addresses eth1 | while read address; do
	    qt $IP -4 rule del from $address
	    run_ip rule add from $address pref 20000 table 2
	    echo "$IP -4 rule del from $address pref 20000 > /dev/null 2>&1" >> ${VARDIR}/undo_std_routing
	    rulenum=$(($rulenum + 1))
	done

	echo 0 > ${VARDIR}/eth1.status

	if [ $COMMAND = enable ]; then
	    rm -f ${VARDIR}/eth1_disabled
	    progress_message2 "Provider std (2) Started"
	else
	    echo 1 > ${VARDIR}/eth1_weight
	    progress_message "Provider std (2) Started"
	fi

    else
	echo 1 > ${VARDIR}/eth1.status
	error_message "WARNING: Interface eth1 is not usable -- Provider std (2) not Started"
    fi
} # End of start_provider_std();

#
# Stop provider std
#
stop_provider_std() {
    if [ -f ${VARDIR}/undo_std_routing ]; then
	. ${VARDIR}/undo_std_routing
	> ${VARDIR}/undo_std_routing
	echo 1 > ${VARDIR}/eth1.status
	progress_message2 "   Provider std (2) stopped"
    else
	startup_error "${VARDIR}/undo_std_routing does not exist"
    fi
}

#
# Enable an optional provider
#
enable_provider() {
    g_interface=$1;

    case $g_interface in

	ppp0|wild)
	    if [ -z "`$IP -4 route ls table 1`" ]; then
		start_provider_wild
	    elif [ -z "$2" ]; then
		startup_error "Interface ppp0 is already enabled"
	    fi
	    ;;
	eth1|std)
	    if [ -z "`$IP -4 route ls table 2`" ]; then
		start_provider_std
	    elif [ -z "$2" ]; then
		startup_error "Interface eth1 is already enabled"
	    fi
	    ;;
	*)
	    startup_error "$g_interface is not an optional provider or interface"
	    ;;
    esac

}

#
# Disable an optional provider
#
disable_provider() {
    g_interface=$1;

    case $g_interface in

	ppp0|wild)
	    if [ -n "`$IP -4 route ls table 1`" ]; then
		stop_provider_wild
	    elif [ -z "$2" ]; then
		startup_error "Interface ppp0 is already disabled"
	    fi
	    ;;
	eth1|std)
	    if [ -n "`$IP -4 route ls table 2`" ]; then
		stop_provider_std
	    elif [ -z "$2" ]; then
		startup_error "Interface eth1 is already disabled"
	    fi
	    ;;
	*)
	    startup_error "$g_interface is not an optional provider interface"
	    ;;
    esac
}

#
# Setup routing and traffic shaping
#
setup_routing_and_traffic_shaping() {
    
    if [ -z "$g_noroutes" ]; then
	#
	# Undo any changes made since the last time that we [re]started -- this will not restore the default route
	#
	undo_routing
	
	#
	# Update the routing table database
	#
	if [ -w /etc/iproute2/rt_tables ]; then
	    cat > /etc/iproute2/rt_tables <<EOF
#
# reserved values
#
255	local
254	main
253	default
0	unspec
#
# local
#
1	wild
2	std
EOF
	else
	    error_message "WARNING: /etc/iproute2/rt_tables is missing or is not writeable"
	fi

	#
	# Capture the default route(s) if we don't have it (them) already.
	#
	[ -f ${VARDIR}/default_route ] || $IP -4 route list | save_default_route > ${VARDIR}/default_route
	
	progress_message2 Adding Providers...

	DEFAULT_ROUTE=
	FALLBACK_ROUTE=

	> ${VARDIR}/undo_main_routing 

	> ${VARDIR}/undo_default_routing 

	start_provider_wild
	start_provider_std

	#
	# We don't have any 'balance'. 'load='  or 'fallback=' providers so we restore any default route that we've saved
	#
	restore_default_route 

	#
	# Delete any routes in the 'balance' table
	#
	while qt $IP -4 route del default table 250; do
	    true
	done

	run_ip route flush cache
    fi

}

#
# This function initializes the global variables used by the program
#
initialize()
{
    #
    # Be sure that umask is sane
    #
    umask 077

    #
    # These variables are required by the library functions called in this script
    #
    g_family=4
    g_confdir=/etc/shorewall
    g_product=Shorewall
    g_program=shorewall
    g_basedir=/usr/share/shorewall
    CONFIG_PATH="/home/user/shorewall/regressionLibrary/5.0.15/wildoptional/:/usr/share/shorewall/:/usr/share/shorewall/deprecated/"
    [ -f ${g_confdir}/vardir ] && . ${g_confdir}/vardir
    [ -n "${VARDIR:=/var/lib/shorewall}" ]
    [ -n "${VARLIB:=/var/lib}" ]
    TEMPFILE=
    DISABLE_IPV6=""
    MODULESDIR=""
    MODULE_SUFFIX="ko"
    LOAD_HELPERS_ONLY="Yes"
    LOCKFILE=""
    SUBSYSLOCK=""
    LOG_VERBOSITY="2"
    RESTART="reload"
    [ -n "${COMMAND:=reload}" ]
    [ -n "${VERBOSITY:=0}" ]
    [ -n "${RESTOREFILE:=restore}" ]
    PATH="/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin:/usr/local/sbin"
    TERMINATOR=fatal_error
    DONT_LOAD=""
    STARTUP_LOG="/var/log/shorewall-init.log"

    [ -z "$IPTABLES" ] && IPTABLES=$(mywhich iptables) # /sbin/shorewall exports IPTABLES
    [ -n "$IPTABLES" -a -x "$IPTABLES" ] || startup_error "Can't find iptables executable"

    case $IPTABLES in
	*/*)
	    ;;
	*)
	    IPTABLES=./$IPTABLES
	    ;;
    esac

    IP6TABLES=${IPTABLES%/*}/ip6tables
    IPTABLES_RESTORE=${IPTABLES}-restore
    [ -x "$IPTABLES_RESTORE" ] || startup_error "$IPTABLES_RESTORE does not exist or is not executable"
    g_tool=$IPTABLES
    IP=ip
    TC=tc
    IPSET=ipset

    g_stopping=

    #
    # The library requires that ${VARDIR} exist
    #
    [ -d ${VARDIR} ] || mkdir -p ${VARDIR}

}

#
# Set global variables holding detected IP information
#
detect_configuration()
{
    local interface

    interface="$1"

    if [ -n "$interface" ]; then
	case $interface in
	    wild)
		interface=ppp0
		;;
	    std)
		interface=eth1
		;;
	esac
    fi

    [ -z "$interface" -o "$interface" = "eth1" ] && SW_ETH1_ADDRESS=$(find_first_interface_address_if_any eth1)

    if [ -z "$interface" -o "$interface" = "eth1" ]; then
	[ -n "$SW_ETH1_GATEWAY" ] || SW_ETH1_GATEWAY=$(detect_gateway eth1)
    fi

    SW_PPP0_IS_USABLE=
    SW_ETH1_IS_USABLE=
    SW_PPP_PLUS_IS_USABLE=

    for interface in $(find_all_interfaces1); do
	case "$interface" in
	    ppp0)
		if [ -z "$interface" -o "$interface" = "ppp0" ]; then
		    if interface_is_usable ppp0; then
			SW_PPP0_IS_USABLE=Yes
			SW_PPP_PLUS_IS_USABLE=Yes
		    fi
		fi

		;;
	    eth1)
		if [ -z "$interface" -o "$interface" = "eth1" ]; then
		    if interface_is_usable eth1 && [ -n "$SW_ETH1_GATEWAY" ]; then
			SW_ETH1_IS_USABLE=Yes
		    fi
		fi

		;;
	    ppp*)
		if [ -z "$SW_PPP_PLUS_IS_USABLE" ]; then
		    if interface_is_usable $interface; then
			SW_PPP_PLUS_IS_USABLE=Yes
		    fi
		fi
		;;
	    *)
		;;
	esac
    done

}

#
# Create the input to iptables-restore/ip6tables-restore and pass that input to the utility
#
setup_netfilter()
{
    local option

    if [ "$COMMAND" = reload -a -n "$g_counters" ] && chain_exists $g_sha1sum1 && chain_exists $g_sha1sum2 ; then
	option="--counters"

	progress_message "Reusing existing ruleset..."

    else
	option=
	
	progress_message2 Preparing iptables-restore input...

	exec 3>${VARDIR}/.iptables-restore-input

	cat >&3 << __EOF__
*raw
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
COMMIT
*nat
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
:ppp+_masq - [0:0]
-A POSTROUTING -o ppp+ -j ppp+_masq
-A ppp+_masq -s 10.0.0.0/8 -j MASQUERADE
-A ppp+_masq -s 169.254.0.0/16 -j MASQUERADE
-A ppp+_masq -s 172.16.0.0/12 -j MASQUERADE
-A ppp+_masq -s 192.168.0.0/16 -j MASQUERADE
COMMIT
*mangle
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
:routemark - [0:0]
-A PREROUTING -j CONNMARK --restore-mark --mask 0xff
-A PREROUTING -i ppp0 -m mark --mark 0/0xff -j routemark
-A PREROUTING -i eth1 -m mark --mark 0/0xff -j routemark
-A FORWARD -j MARK --set-mark 0/0xff
-A OUTPUT -j CONNMARK --restore-mark --mask 0xff
-A routemark -i ppp0 -j MARK --set-mark 0x1/0xff
-A routemark -i eth1 -j MARK --set-mark 0x2/0xff
-A routemark -m mark ! --mark 0/0xff -j CONNMARK --save-mark --mask 0xff
COMMIT
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT DROP [0:0]
:Broadcast - [0:0]
:Drop - [0:0]
:Reject - [0:0]
:dynamic - [0:0]
:eth1_fwd - [0:0]
:eth1_in - [0:0]
:eth1_out - [0:0]
:fw2loc - [0:0]
:fw2net - [0:0]
:loc2fw - [0:0]
:loc_frwd - [0:0]
:logdrop - [0:0]
:logflags - [0:0]
:logreject - [0:0]
:net2fw - [0:0]
:net2loc - [0:0]
:ppp+_fwd - [0:0]
:ppp+_in - [0:0]
:ppp+_out - [0:0]
:reject - [0:0]
:sfilter - [0:0]
:smurflog - [0:0]
:smurfs - [0:0]
:tcpflags - [0:0]
:$g_sha1sum1 - [0:0]
:$g_sha1sum2 - [0:0]
-A INPUT -i ppp+ -j ppp+_in
-A INPUT -i eth1 -j eth1_in
-A INPUT -i eth0 -j loc2fw
-A INPUT -i lo -j ACCEPT
-A INPUT -j Reject
-A INPUT -j LOG --log-level 6 --log-prefix "Shorewall:INPUT:REJECT:"
-A INPUT -g reject
-A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu
-A FORWARD -i ppp+ -j ppp+_fwd
-A FORWARD -i eth1 -j eth1_fwd
-A FORWARD -i eth0 -j loc_frwd
-A FORWARD -j Reject
-A FORWARD -j LOG --log-level 6 --log-prefix "Shorewall:FORWARD:REJECT:"
-A FORWARD -g reject
-A OUTPUT -o ppp+ -j ppp+_out
-A OUTPUT -o eth1 -j eth1_out
-A OUTPUT -o eth0 -j fw2loc
-A OUTPUT -o lo -j ACCEPT
-A OUTPUT -j Reject
-A OUTPUT -j LOG --log-level 6 --log-prefix "Shorewall:OUTPUT:REJECT:"
-A OUTPUT -g reject
-A Broadcast -m addrtype --dst-type BROADCAST -j DROP
-A Broadcast -m addrtype --dst-type MULTICAST -j DROP
-A Broadcast -m addrtype --dst-type ANYCAST -j DROP
-A Drop
-A Drop -p 1 --icmp-type 3/4 -j ACCEPT -m comment --comment "Needed ICMP types"
-A Drop -p 1 --icmp-type 11 -j ACCEPT -m comment --comment "Needed ICMP types"
-A Drop -j Broadcast
-A Drop -m conntrack --ctstate INVALID -j DROP
-A Drop -p 17 -m multiport --dports 135,445 -j DROP -m comment --comment "SMB"
-A Drop -p 17 --dport 137:139 -j DROP -m comment --comment "SMB"
-A Drop -p 17 --dport 1024:65535 --sport 137 -j DROP -m comment --comment "SMB"
-A Drop -p 6 -m multiport --dports 135,139,445 -j DROP -m comment --comment "SMB"
-A Drop -p 17 --dport 1900 -j DROP -m comment --comment "UPnP"
-A Drop -p 6 ! --syn -j DROP
-A Drop -p 17 --sport 53 -j DROP -m comment --comment "Late DNS Replies"
-A Reject
-A Reject -p 1 --icmp-type 3/4 -j ACCEPT -m comment --comment "Needed ICMP types"
-A Reject -p 1 --icmp-type 11 -j ACCEPT -m comment --comment "Needed ICMP types"
-A Reject -j Broadcast
-A Reject -m conntrack --ctstate INVALID -j DROP
-A Reject -p 17 -m multiport --dports 135,445 -g reject -m comment --comment "SMB"
-A Reject -p 17 --dport 137:139 -g reject -m comment --comment "SMB"
-A Reject -p 17 --dport 1024:65535 --sport 137 -g reject -m comment --comment "SMB"
-A Reject -p 6 -m multiport --dports 135,139,445 -g reject -m comment --comment "SMB"
-A Reject -p 17 --dport 1900 -j DROP -m comment --comment "UPnP"
-A Reject -p 6 ! --syn -j DROP
-A Reject -p 17 --sport 53 -j DROP -m comment --comment "Late DNS Replies"
__EOF__

	[ -f ${VARDIR}/.dynamic ] && cat ${VARDIR}/.dynamic >&3

	cat >&3 << __EOF__
-A eth1_fwd -o eth1 -g sfilter
-A eth1_fwd -m conntrack --ctstate NEW,INVALID,UNTRACKED -j dynamic
-A eth1_fwd -m conntrack --ctstate NEW,INVALID,UNTRACKED -j smurfs
-A eth1_fwd -p tcp -j tcpflags
-A eth1_fwd -o ppp+ -j ACCEPT
-A eth1_fwd -o eth1 -j ACCEPT
-A eth1_fwd -o eth0 -j net2loc
-A eth1_in -m conntrack --ctstate NEW,INVALID,UNTRACKED -j dynamic
-A eth1_in -m conntrack --ctstate NEW,INVALID,UNTRACKED -j smurfs
-A eth1_in -p udp --dport 67:68 -j ACCEPT
-A eth1_in -p tcp -j tcpflags
-A eth1_in -j net2fw
-A eth1_out -p udp --dport 67:68 -j ACCEPT
-A eth1_out -j fw2net
-A fw2loc -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A fw2loc -p 1 -j ACCEPT
-A fw2loc -j Reject
-A fw2loc -j LOG --log-level 6 --log-prefix "Shorewall:fw2loc:REJECT:"
-A fw2loc -g reject
-A fw2net -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A fw2net -p 17 --dport 53 -j ACCEPT -m comment --comment "DNS"
-A fw2net -p 6 --dport 53 -j ACCEPT -m comment --comment "DNS"
-A fw2net -p 1 -j ACCEPT
-A fw2net -j Reject
-A fw2net -j LOG --log-level 6 --log-prefix "Shorewall:fw2net:REJECT:"
-A fw2net -g reject
-A loc2fw -m conntrack --ctstate NEW,INVALID,UNTRACKED -j dynamic
-A loc2fw -m conntrack --ctstate NEW,INVALID,UNTRACKED -j smurfs
-A loc2fw -p tcp -j tcpflags
-A loc2fw -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A loc2fw -p 6 --dport 22 -j ACCEPT -m comment --comment "SSH"
-A loc2fw -p 1 --icmp-type 8 -j ACCEPT -m comment --comment "Ping"
-A loc2fw -j Reject
-A loc2fw -j LOG --log-level 6 --log-prefix "Shorewall:loc2fw:REJECT:"
-A loc2fw -g reject
-A loc_frwd -m conntrack --ctstate NEW,INVALID,UNTRACKED -j dynamic
-A loc_frwd -m conntrack --ctstate NEW,INVALID,UNTRACKED -j smurfs
-A loc_frwd -p tcp -j tcpflags
-A loc_frwd -o ppp+ -j ACCEPT
-A loc_frwd -o eth1 -j ACCEPT
-A logdrop -j DROP
-A logflags -j LOG --log-ip-options --log-level 6 --log-prefix "Shorewall:logflags:DROP:"
-A logflags -j DROP
-A logreject -j reject
-A net2fw -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A net2fw -p 1 --icmp-type 8 -j DROP -m comment --comment "Ping"
-A net2fw -j Drop
-A net2fw -j LOG --log-level 6 --log-prefix "Shorewall:net2fw:DROP:"
-A net2fw -j DROP
-A net2loc -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A net2loc -j Drop
-A net2loc -j LOG --log-level 6 --log-prefix "Shorewall:net2loc:DROP:"
-A net2loc -j DROP
-A ppp+_fwd -o ppp+ -g sfilter
-A ppp+_fwd -m conntrack --ctstate NEW,INVALID,UNTRACKED -j dynamic
-A ppp+_fwd -m conntrack --ctstate NEW,INVALID,UNTRACKED -j smurfs
-A ppp+_fwd -p tcp -j tcpflags
-A ppp+_fwd -o ppp+ -j ACCEPT
-A ppp+_fwd -o eth1 -j ACCEPT
-A ppp+_fwd -o eth0 -j net2loc
-A ppp+_in -m conntrack --ctstate NEW,INVALID,UNTRACKED -j dynamic
-A ppp+_in -m conntrack --ctstate NEW,INVALID,UNTRACKED -j smurfs
-A ppp+_in -p udp --dport 67:68 -j ACCEPT
-A ppp+_in -p tcp -j tcpflags
-A ppp+_in -j net2fw
-A ppp+_out -p udp --dport 67:68 -j ACCEPT
-A ppp+_out -j fw2net
-A reject -m addrtype --src-type BROADCAST -j DROP
-A reject -s 224.0.0.0/4 -j DROP
-A reject -p 2 -j DROP
-A reject -p 6 -j REJECT --reject-with tcp-reset
-A reject -p 17 -j REJECT
-A reject -p 1 -j REJECT --reject-with icmp-host-unreachable
-A reject -j REJECT --reject-with icmp-host-prohibited
-A sfilter -j LOG --log-level 6 --log-prefix "Shorewall:sfilter:DROP:"
-A sfilter -j DROP
-A smurflog -j LOG --log-level 6 --log-prefix "Shorewall:smurfs:DROP:"
-A smurflog -j DROP
-A smurfs -s 0.0.0.0 -j RETURN
-A smurfs -m addrtype --src-type BROADCAST -g smurflog
-A smurfs -s 224.0.0.0/4 -g smurflog
-A tcpflags -p tcp --tcp-flags ALL FIN,URG,PSH -g logflags
-A tcpflags -p tcp --tcp-flags ALL NONE -g logflags
-A tcpflags -p tcp --tcp-flags SYN,RST SYN,RST -g logflags
-A tcpflags -p tcp --tcp-flags FIN,RST FIN,RST -g logflags
-A tcpflags -p tcp --tcp-flags SYN,FIN SYN,FIN -g logflags
-A tcpflags -p tcp --tcp-flags ACK,PSH,FIN PSH,FIN -g logflags
-A tcpflags -p tcp --syn --sport 0 -g logflags
COMMIT
__EOF__

    fi

    exec 3>&-

    [ -n "$g_debug_iptables" ] && command=debug_restore_input || command="$IPTABLES_RESTORE $option"

    progress_message2 "Running $command..."

    cat ${VARDIR}/.iptables-restore-input | $command # Use this nonsensical form to appease SELinux
    if [ $? != 0 ]; then
	fatal_error "iptables-restore Failed. Input is in ${VARDIR}/.iptables-restore-input"
    fi

}

chainlist_reload()
{
    true
}

#
#Save the ipsets specified by the SAVE_IPSETS setting and by dynamic zones and blacklisting
#
save_ipsets() {
    true
}

#
#Flush and Destroy the sets then load fresh copy from a saved ipset file
#
load_ipsets() {
    true
}
#
# Start/Reload the Firewall
#
define_firewall() {
    local options
    
    progress_message2 Initializing...

    echo MODULESDIR="$MODULESDIR" > ${VARDIR}/.modulesdir
    cat > ${VARDIR}/.modules << EOF
loadmodule ip_conntrack_amanda
loadmodule ip_conntrack_ftp
loadmodule ip_conntrack_h323
loadmodule ip_conntrack_irc
loadmodule ip_conntrack_netbios_ns
loadmodule ip_conntrack_pptp
loadmodule ip_conntrack_sip
loadmodule ip_conntrack_tftp
loadmodule ip_nat_amanda
loadmodule ip_nat_ftp
loadmodule ip_nat_h323
loadmodule ip_nat_irc
loadmodule ip_nat_pptp
loadmodule ip_nat_sip
loadmodule ip_nat_snmp_basic
loadmodule ip_nat_tftp
loadmodule nf_conntrack_ftp
loadmodule nf_conntrack_h323
loadmodule nf_conntrack_irc
loadmodule nf_conntrack_netbios_ns
loadmodule nf_conntrack_netlink
loadmodule nf_conntrack_pptp
loadmodule nf_conntrack_proto_gre
loadmodule nf_conntrack_proto_sctp
loadmodule nf_conntrack_proto_udplite
loadmodule nf_conntrack_sip         sip_direct_media=0
loadmodule nf_conntrack_tftp
loadmodule nf_conntrack_sane
loadmodule nf_nat_amanda
loadmodule nf_nat_ftp
loadmodule nf_nat_h323
loadmodule nf_nat_irc
loadmodule nf_nat
loadmodule nf_nat_pptp
loadmodule nf_nat_proto_gre
loadmodule nf_nat_sip
loadmodule nf_nat_snmp_basic
loadmodule nf_nat_tftp
loadmodule ipt_LOG
loadmodule nf_log_ipv4
loadmodule xt_LOG
loadmodule xt_NFLOG
loadmodule ipt_ULOG
loadmodule nfnetlink_log
EOF

    reload_kernel_modules < ${VARDIR}/.modules

    if [ "$COMMAND" = refresh ]; then
       run_refresh_exit
    else
	run_init_exit
    fi

    load_ipsets

    if [ "$COMMAND" = reload -o "$COMMAND" = refresh ]; then
	if [ -n "$g_counters" ]; then
	    ${IPTABLES}-save --counters | grep -vE '[ :]shorewall ' > ${VARDIR}/.iptables-restore-input
	fi

	if chain_exists 'UPnP -t nat'; then
	    iptables-restore -t nat | grep '^-A UPnP ' > ${VARDIR}/.UPnP
	else
	    rm -f ${VARDIR}/.UPnP
	fi
	
	if chain_exists forwardUPnP; then
	    iptables-restore -t filter | grep '^-A forwardUPnP ' > ${VARDIR}/.forwardUPnP
	else
	    rm -f ${VARDIR}/.forwardUPnP
	fi
	
	if chain_exists dynamic; then
	    iptables-restore -t filter | grep '^-A dynamic ' > ${VARDIR}/.dynamic
	else
	    rm -f ${VARDIR}/.dynamic
	fi

    else
	rm -f ${VARDIR}/.UPnP
	rm -f ${VARDIR}/.forwardUPnP

    fi

    qt1 $IPTABLES -L shorewall -n && qt1 $IPTABLES -F shorewall && qt1 $IPTABLES -X shorewall

    delete_proxyarp

    if [ -f ${VARDIR}/nat ]; then
	while read external interface; do
	    del_ip_addr $external $interface
	done < ${VARDIR}/nat

	rm -f ${VARDIR}/nat
    fi

    delete_tc1

    setup_common_rules

    setup_routing_and_traffic_shaping

    cat > ${VARDIR}/proxyarp << __EOF__
__EOF__

    if [ "$COMMAND" != refresh ]; then
	cat > ${VARDIR}/zones << __EOF__
fw firewall
net ipv4 eth1:0.0.0.0/0 ppp+:0.0.0.0/0
loc ipv4 eth0:0.0.0.0/0
__EOF__
	cat > ${VARDIR}/policies << __EOF__
fw 	=>	net	REJECT using chain fw2net
fw 	=>	loc	REJECT using chain fw2loc
net 	=>	fw	DROP using chain net2fw
net 	=>	loc	DROP using chain net2loc
loc 	=>	fw	REJECT using chain loc2fw
loc 	=>	net	ACCEPT
__EOF__
	cat > ${VARDIR}/marks << __EOF__
Traffic Shaping:0-16383 (0x0-0x3fff) mask 0xffff
User: Not Enabled
Provider:1-255 (0x1-0xff) mask 0xff
Zone: Not Enabled
Exclusion:65536 mask 0x10000
TProxy:131072 mask 0x20000
__EOF__
    fi

    > ${VARDIR}/nat

    if [ $COMMAND = restore ]; then
	iptables_save_file=${VARDIR}/$(basename $0)-iptables
	if [ -f $iptables_save_file ]; then
	    [ -n "$g_counters" ] && options=--counters
	    cat $iptables_save_file | $IPTABLES_RESTORE $options # Use this nonsensical form to appease SELinux
	else
	   fatal_error "$iptables_save_file does not exist"
	fi

	echo 1 > /proc/sys/net/ipv4/ip_forward
	progress_message2 IPv4 Forwarding Enabled

	set_state Started /home/user/shorewall/regressionLibrary/5.0.15/wildoptional/
	run_restored_exit
    elif [ $COMMAND = refresh ]; then
	chainlist_reload

	echo 1 > /proc/sys/net/ipv4/ip_forward
	progress_message2 IPv4 Forwarding Enabled

	run_refreshed_exit
	do_iptables -N shorewall
	do_iptables -A shorewall -m recent --set --name %CURRENTTIME
	set_state Started /home/user/shorewall/regressionLibrary/5.0.15/wildoptional/
	[ $0 = ${VARDIR}/firewall ] || cp -f $(my_pathname) ${VARDIR}/firewall
    else
	setup_netfilter
	conditionally_flush_conntrack

	echo 1 > /proc/sys/net/ipv4/ip_forward
	progress_message2 IPv4 Forwarding Enabled

	run_start_exit
	do_iptables -N shorewall

	do_iptables -A shorewall -m recent --set --name %CURRENTTIME
	set_state Started /home/user/shorewall/regressionLibrary/5.0.15/wildoptional/
	my_pathname=$(my_pathname)
	[ $my_pathname = ${VARDIR}/firewall ] || cp -f $my_pathname ${VARDIR}/firewall
	run_started_exit
    fi

    date > ${VARDIR}/restarted
    
    case $COMMAND in
	start)
	    mylogger kern.info "$g_product started"
	    ;;
	reload)
	    mylogger kern.info "$g_product reloaded"
	    ;;
	refresh)
	    mylogger kern.info "$g_product refreshed"
	    ;;
	restore)
	    mylogger kern.info "$g_product restored"
	    ;;
    esac

}

#
# Stop/restore the firewall after an error or because of a 'stop' or 'clear' command
#
stop_firewall() {

    deletechain() {
	qt $IPTABLES -L $1 -n && qt $IPTABLES -F $1 && qt $IPTABLES -X $1
    }

    case $COMMAND in
	stop|clear|restore)
	    if chain_exists dynamic; then
		${IPTABLES}-save -t filter | grep '^-A dynamic' | fgrep -v -- '-j ACCEPT' > ${VARDIR}/.dynamic
	    fi
	    ;;
	*)
	    set +x

	    case $COMMAND in
		start)
		    mylogger kern.err "ERROR:$g_product start failed"
		    ;;
		reload)
		    mylogger kern.err "ERROR:$g_product reload failed"
		    ;;
		refresh)
		    mylogger kern.err "ERROR:$g_product refresh failed"
		    ;;
		enable)
		    mylogger kern.err "ERROR:$g_product 'enable $g_interface' failed"
		    ;;
	    esac

	    if [ "$RESTOREFILE" = NONE ]; then
		COMMAND=clear
		clear_firewall
		echo "$g_product Cleared"

		kill $$
		exit 2
	    else
		g_restorepath=${VARDIR}/$RESTOREFILE

		if [ -x $g_restorepath ]; then
		    echo Restoring ${g_product:=Shorewall}...

		    g_recovering=Yes

		    if run_it $g_restorepath restore; then
			echo "$g_product restored from $g_restorepath"
			set_state "Restored from $g_restorepath"
		    else
			set_state "Unknown"
		    fi

		    kill $$
		    exit 2
		fi
	    fi
	    ;;
    esac

    if [ -n "$g_stopping" ]; then
	kill $$
	exit 1
    fi

    set_state "Stopping"

    g_stopping="Yes"

    deletechain shorewall

    run_stop_exit

    #
    # Enable automatic helper association on kernel 3.5.0 and later
    #
    if [ $COMMAND = clear -a -f /proc/sys/net/netfilter/nf_conntrack_helper ]; then
	echo 1 > /proc/sys/net/netfilter/nf_conntrack_helper
    fi

    if [ -f ${VARDIR}/nat ]; then
	while read external interface; do
      	    del_ip_addr $external $interface
	done < ${VARDIR}/nat

	rm -f ${VARDIR}/nat
    fi

    if [ -f ${VARDIR}/proxyarp ]; then
	while read address interface external haveroute; do
	    qtnoin $IP -4 neigh del proxy $address dev $external
	    [ -z "${haveroute}${g_noroutes}" ] && qtnoin $IP -4 route del $address/32 dev $interface
	    f=/proc/sys/net/ipv4/conf/$interface/proxy_arp
	    [ -f $f ] && echo 0 > $f
	done < ${VARDIR}/proxyarp

	rm -f ${VARDIR}/proxyarp
    fi


    delete_tc1
    undo_routing
    restore_default_route 

    progress_message2 Preparing iptables-restore input...

    exec 3>${VARDIR}/.iptables-restore-stop-input

    cat >&3 << __EOF__
*raw
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
COMMIT
*nat
:PREROUTING ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
COMMIT
*mangle
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
COMMIT
*filter
:INPUT DROP [0:0]
:FORWARD DROP [0:0]
:OUTPUT ACCEPT [0:0]
-A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A INPUT -i eth0 -j ACCEPT
-A INPUT -i lo -j ACCEPT
-A INPUT -p udp --dport 67:68 -i ppp+ -j ACCEPT
-A INPUT -p udp --dport 67:68 -i eth1 -j ACCEPT
-A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
-A FORWARD -p udp --dport 67:68 -i ppp+ -o ppp+ -j ACCEPT
-A FORWARD -p udp --dport 67:68 -i eth1 -o eth1 -j ACCEPT
COMMIT
__EOF__

    [ -n "$g_debug_iptables" ] && command=debug_restore_input || command=$IPTABLES_RESTORE

    progress_message2 "Running $command..."

    cat ${VARDIR}/.iptables-restore-stop-input | $command # Use this nonsensical form to appease SELinux

    if [ $? != 0 ]; then
	error_message "ERROR: $command Failed."
    fi

    echo 1 > /proc/sys/net/ipv4/ip_forward
    progress_message2 IPv4 Forwarding Enabled

    rm -f ${VARDIR}/*.address
    rm -f ${VARDIR}/*.gateway

    run_stopped_exit


    set_state "Stopped"
    mylogger kern.info "$g_product Stopped"

    case $COMMAND in
    stop|clear)
	;;
    *)
	#
	# The firewall is being stopped when we were trying to do something
	# else. Kill the shell in case we're running in a subshell
	#
	kill $$
	;;
    esac
}

#
# Handle the "up" and "down" commands
#
updown() # $1 = interface
{
    local state
    state=cleared

    progress_message3 "$g_product $COMMAND triggered by $1"

    if shorewall_is_started; then
	state=started
    elif [ -f ${VARDIR}/state ]; then
	case "$(cat ${VARDIR}/state)" in
	    Stopped*)
		state=stopped
		;;
	    Cleared*)
		;;
	    *)
		state=unknown
		;;
	esac
    else
	state=unknown
    fi

    case $1 in
	lo)
	    progress_message3 "$COMMAND on interface $1 ignored"
	    exit 0
	    ;;
	ppp0|eth1)
	    if [ "$state" = started ]; then
		if [ "$COMMAND" = up ]; then
		    progress_message3 "Attempting enable on interface $1"
		    COMMAND=enable
		    detect_configuration $1
		    enable_provider $1
		elif [ "$PHASE" != post-down ]; then # pre-down or not Debian
		    progress_message3 "Attempting disable on interface $1"
		    COMMAND=disable
		    detect_configuration $1
		    disable_provider $1
		fi
	    elif [ "$COMMAND" = up ]; then
		echo 0 > ${VARDIR}/${1}.status
		COMMAND=start
		progress_message3 "$g_product attempting start"
		detect_configuration
		define_firewall
	    else
		progress_message3 "$COMMAND on interface $1 ignored"
	    fi
	    ;;
	ppp*)
	    if [ "$COMMAND" = up ]; then
		echo 0 > ${VARDIR}/${1}.state
	    else
		echo 1 > ${VARDIR}/${1}.state
	    fi

	    if [ "$state" = started ]; then
		COMMAND=reload
		progress_message3 "$g_product attempting reload"
		detect_configuration
		define_firewall
	    elif [ "$state" = stopped ]; then
		COMMAND=start
		progress_message3 "$g_product attempting start"
		detect_configuration
		define_firewall
	    else
		progress_message3 "$COMMAND on interface $1 ignored"
	    fi
	    ;;
	eth0)
	    case $state in
		started)
		    COMMAND=reload
		    progress_message3 "$g_product attempting reload"
		    detect_configuration
		    define_firewall
		    ;;
		*)
		    progress_message3 "$COMMAND on interface $1 ignored"
		    ;;
	    esac
    esac
}

