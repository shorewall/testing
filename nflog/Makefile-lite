#     Shorewall Packet Filtering Firewall Export Directory Makefile - V3.3
#
#     This program is under GPL [http://www.gnu.org/copyleft/gpl.htm]
#
#     (c) 2006 - Tom Eastep (teastep@shorewall.net)
#
#	Shorewall documentation is available at http://www.shorewall.net
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of Version 2 of the GNU General Public License
#	as published by the Free Software Foundation.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA
################################################################################
# Place this file in each export directory. Modify each copy to set HOST
# to the name of the remote firewall corresponding to the directory.
#
#	To make the 'firewall' script, type "make".
# 
#	Once the script is compiling correctly, you can install it by
#	typing "make install".
#  
################################################################################
#                             V A R I A B L E S
#
# Files in the export directory on which the firewall script does not depend
#
IGNOREFILES =  firewall% Makefile% trace% %~
#
# Remote Firewall system
#
HOST = gateway
#
# Save some typing
#
LITEDIR = /var/lib/shorewall-lite
#
# Default target is the firewall script
#
################################################################################
#                                T A R G E T S
#
all: firewall
#
# Only generate the capabilities file if it doesn't already exist
#
capabilities: 
	ssh root@$(HOST) "/sbin/shorewall-lite show -f capabilities > $(LITEDIR)/capabilities"
	scp root@$(HOST):$(LITEDIR)/capabilities .
#
# Compile the firewall script. Using the 'wildcard' function causes "*" to be expanded so that
# 'filter-out' will be presented with the list of files in this directory rather than "*"
#
firewall: $(filter-out $(IGNOREFILES) capabilities , $(wildcard *) ) capabilities
	shorewall compile -e . firewall
#
# Only reload on demand.
#
install: firewall
	scp firewall firewall.conf root@$(HOST):$(LITEDIR)
	ssh root@$(HOST) "/sbin/shorewall-lite restart"
#
# Save running configuration
#
save:
	ssh root@$(HOST) "/sbin/shorewall-lite save"
#
# Remove generated files
#
clean: 
	rm -f capabilities firewall firewall.conf reload
